import "./index.scss";

import classNames from "classnames";
import { Textarea, TextareaProps, View } from "@tarojs/components";
import { useState } from "react";

const baseCls = "lxf-textarea";

interface ILxfTextAreaProps extends TextareaProps {
    className?: string;
    [key: string]: any;
}

const LxfTextarea = (props: ILxfTextAreaProps) => {
    const { className, ...rest } = props;
    const [count, setCount] = useState<number>(0);
    return (
        <View className={classNames(baseCls, className)}>
            <Textarea
                placeholderClass={`${baseCls}-placeholder`}
                maxlength={-1}
                onInput={(e) => {
                    const value = e.target.value;
                    setCount(value.length);
                }}
                {...rest}
            />
            <View className={`${baseCls}-count`}>
                {count}/{rest.limit || 200}
            </View>
        </View>
    );
};

export default LxfTextarea;

import './index.scss'

import classNames from 'classnames'
import { Radio, RadioProps, View } from '@tarojs/components'

const baseCls = 'lxf-radio';

interface ILxfRadioProps extends RadioProps {
    value: any;
    label: React.ReactNode;
    className?: string;
}

const LxfRadio = (props: ILxfRadioProps) => {
    const { value, label, className, ...rest } = props;
    return (
        <Radio className={classNames(baseCls, className)} color={"#FE2C55"} value={value} {...rest}>
            <View className={`${baseCls}-text`}>
                {label}
            </View>
        </Radio>
    )
}

export default LxfRadio;
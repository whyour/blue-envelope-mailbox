import './index.scss'

import classNames from 'classnames'
import { Checkbox, CheckboxProps, View } from '@tarojs/components'

const baseCls = 'lxf-checkbox';

interface ILxfCheckboxProps extends CheckboxProps {
    label: React.ReactNode;
    className?: string;
}

const LxfCheckbox = (props: ILxfCheckboxProps) => {
    const { label, className, ...rest } = props;
    return (
        <Checkbox className={classNames(baseCls, className)} color={"#FE2C55"} {...rest}>
            <View className={`${baseCls}-text`}>
                {label}
            </View>
        </Checkbox>
    )
}

export default LxfCheckbox;
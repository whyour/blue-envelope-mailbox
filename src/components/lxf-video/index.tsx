import './index.scss';

import classNames from 'classnames';
import { Video, VideoProps, View } from '@tarojs/components';

const baseCls = 'lxf-video';

interface ILxfTextVideoProps extends VideoProps {
    className?: string;
    [key: string]: any;
}

const LxfVideo = (props: ILxfTextVideoProps) => {
    const { className, ...rest } = props;
    return (
        <View className={classNames(baseCls, className)}>
            <Video
                posterSize={'contain'}
                initialTime={0}
                controls={true}
                autoplay={false}
                loop={false}
                muted={false}
                className={`${baseCls}-video`}
                {...rest}
            />
        </View>
    );
};

export default LxfVideo;

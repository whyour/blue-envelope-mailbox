import './index.scss';

import classNames from 'classnames';
import { useMemo } from 'react';
import { Image, Text, View } from '@tarojs/components';
import closeIcon from '@/assets/icons/close.svg';
import backIcon from '@/assets/icons/arrow-left.svg';

import { IFloatBaseProps, useFloatContentRenderable } from '../lxf-float-wrapper';

export type IDrawerProps = IFloatBaseProps & {
    position?: 'left' | 'top' | 'right' | 'bottom';
    width?: number | string;
    height?: number | string;
    logo?: string;
    showBack?: boolean;
};

const baseCls = 'common-drawer';

const LxfDrawer: React.FunctionComponent<IDrawerProps> = (props) => {
    const {
        style,
        visible,
        title,
        hideTitle,
        showBack,
        onOk,
        okText,
        showOkBtn,
        onCancel,
        cancelText,
        showCancelBtn,
        closable = true,
        maskClosable,
        footer,
        children,
        position = 'bottom',
        width = '75vw',
        height,
        logo,
        forceRender,
        className
    } = props;

    const renderable = useFloatContentRenderable(visible ?? false, forceRender);

    const calcStyle = useMemo(() => {
        switch (position) {
            case 'left':
            case 'right':
                return {
                    width,
                    height: '100vh'
                };
            case 'top':
            case 'bottom':
                return {
                    width: '100vw',
                    height
                };
            default:
                return {};
        }
    }, [position, width, height]);

    return (
        <>
            <View
                className={classNames(baseCls, className, {
                    [`${baseCls}--${position}`]: true,
                    show: visible
                })}
                style={{
                    ...style,
                    ...calcStyle
                }}
            >
                <View className={`${baseCls}-top`}>
                    {showBack && (
                        <View className={`${baseCls}-back-wrapper`} onClick={() => onCancel?.('close')}>
                            <Image className={`${baseCls}-back`} src={backIcon} />
                        </View>
                    )}
                    <View className={`${baseCls}-title`}>{title}</View>
                </View>

                <View className={`${baseCls}-content`}>{renderable && children}</View>

                {footer !== undefined
                    ? footer
                    : (showOkBtn || showCancelBtn) && (
                          <View className={`${baseCls}-bottom`}>
                              {showCancelBtn && (
                                  <View className={`${baseCls}-btn`} onClick={onCancel}>
                                      {cancelText || '取消'}
                                  </View>
                              )}
                              {showOkBtn && (
                                  <View
                                      className={classNames(`${baseCls}-btn`, {
                                          primary: true
                                      })}
                                      onClick={onOk}
                                  >
                                      {okText || '确认'}
                                  </View>
                              )}
                          </View>
                      )}

                {closable && (
                    <View className={`${baseCls}-close`} onClick={() => onCancel?.('close')}>
                        <Image className={`${baseCls}-close-icon`} src={closeIcon} />
                    </View>
                )}
            </View>
            <View
                // @ts-ignore
                catchMove
                className={classNames(`${baseCls}-mask`, {
                    show: visible
                })}
                onClick={() => maskClosable && onCancel?.('close')}
            />
        </>
    );
};

export default LxfDrawer;

import "./index.scss";

import classNames from "classnames";
import { Picker, View, Image, Text } from "@tarojs/components";
import {
    PickerDateProps,
    PickerMultiSelectorProps,
    PickerRegionProps,
    PickerSelectorProps,
    PickerTimeProps,
} from "@tarojs/components/types/Picker";
import IconPickerArrow from "./assest/picker-arrow.svg";

const baseCls = "lxf-picker";

type ILxfPickerProps = (
    | PickerTimeProps
    | PickerDateProps
    | PickerRegionProps
    | PickerSelectorProps
    | PickerMultiSelectorProps
) & {
    placeholder: string;
};

const LxfPicker = (props: ILxfPickerProps) => {
    const { children, className, placeholder, ...rest } = props;

    return (
        <Picker className={classNames(baseCls, className)} {...rest}>
            <View className={`${baseCls}-value`}>
                {children || placeholder}
            </View>
            <Image className={`${baseCls}-icon`} src={IconPickerArrow} />
        </Picker>
    );
};

export default LxfPicker;

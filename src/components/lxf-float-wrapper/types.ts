import React, { ReactNode } from 'react'

import { IDrawerProps } from '../lxf-drawer'
import { IModalProps } from '../lxf-modal';
import { IBaseComponentProps } from '../types'

export type IFloatBaseProps = IBaseComponentProps & {
  /** 是否显示 */
  visible?: boolean;
  /** 标题 */
  title?: React.ReactNode;
  /** 标题 */
  hideTitle?: boolean;
  /** 确认回调 */
  onOk?: (...args: any[]) => void | Promise<void>;
  /** 确认文案 */
  okText?: React.ReactNode;
  /** 隐藏确认按钮 */
  showOkBtn?: boolean;
  /** 取消回调 */
  onCancel?: (...args: any) => void;
  /** 取消文案 */
  cancelText?: React.ReactNode;
  /** 隐藏取消按钮 */
  showCancelBtn?: boolean;
  /** 按钮关闭 */
  closable?: boolean;
  /** 蒙层关闭 */
  maskClosable?: boolean;
  /** 底部按钮 */
  footer?: ReactNode;
  /** 强制渲染 */
  forceRender?: boolean;
};

export type IFloatWrapperBaseProps = {
  /** 标题 */
  title?: React.ReactNode;
};

export type IFloatWrapperOpenProps<P = {}> = IFloatWrapperBaseProps & {
  /** 内容属性 */
  props?: P;
};

export type IFloatWrapperProps<P = {}> = IFloatWrapperBaseProps & {
  /** 浮层容器显隐状态 */
  visible?: boolean;
  /** 浮层容器提交回调函数 */
  onOk?: (result?: any, componentProps?: P) => void | Promise<void>;
  /** 浮层容器取消回调函数 */
  onCancel?: (...args: any) => void;
  /** 内容属性 */
  componentProps?: P;
}

export type IFloatWrappedModalProps<P = {}> = Omit<
  IModalProps,
  'onOk' | 'onCancel'
> &
  IFloatWrapperProps<P>;

export type IFloatWrappedDrawerProps<P = {}> = Omit<
  IDrawerProps,
  'onOk' | 'onCancel'
> &
  IFloatWrapperProps<P>;

export type IFloatWrapperRef<P = {}> = {
  /** 浮层弹窗打开接口 */
  open: (openProps?: IFloatWrapperOpenProps<P>) => void;
  /** 浮层弹窗关闭接口 */
  close: () => void;
};

export type IFloatWrappedComponentRef = {
  /** 浮层容器提交前被包裹组件回调函数 */
  beforeOk?: (result?: any) => any;
};

export type IWithFloatWrapperOptions<P = {}> = {
  /** 默认属性 */
  defaultProps?: Partial<IFloatWrapperProps<P>>;
};

export type IWithFloatWrapperProps<P = {}> = IFloatWrapperBaseProps &
  P & {
    /** 浮层容器显隐状态 */
    visible?: boolean;
    /** 浮层容器提交回调函数 */
    onOk?: (result?: any) => Promise<void>;
    /** 浮层容器取消回调函数 */
    onCancel?: (...args: any) => void;
  };

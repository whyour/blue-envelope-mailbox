import './index.scss'

import classNames from 'classnames'
import { ButtonProps, View } from '@tarojs/components'

import LxfButton from '../lxf-button'

const baseCls = 'panel';

interface ILxfPanelProps {
    title: string;
    disabled?: boolean;
    content?: string;
    className?: string;
    buttonText?: string;
    type?: 'info' | 'action';
    onClick?: () => void;
    buttonProps?: ButtonProps;
}

const LxfPanel = (props: ILxfPanelProps) => {
    const { title, content, disabled = false, type = 'action', buttonText, className, onClick, buttonProps } = props;
    return (
        <View className={classNames(baseCls, className, type)}>
            <View className={`${baseCls}-left`}>
                <View className={'lxf_h3_text'}> {title} </View>
                <View className={`lxf_info_text_2`}> {content} </View>
            </View>
        {buttonText && <LxfButton className={`${baseCls}-right`} title={buttonText} disabled={disabled} {...buttonProps} onClick={onClick}/>}
        </View>
    )
}

export default LxfPanel;
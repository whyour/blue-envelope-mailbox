import './index.scss'

import classNames from 'classnames'
import { CheckboxGroup, CheckboxGroupProps } from '@tarojs/components'

import LxfCheckbox from '../lxf-checkbox'

const baseCls = 'lxf-checkbox-group';
interface ILxfCheckboxGroupOption<T> {
    label: React.ReactNode;
    value: T;
}

interface ILxfCheckboxGroupProps<T> extends CheckboxGroupProps {
    options?: ILxfCheckboxGroupOption<T>[];
    className?: string;
}

const LxfCheckboxGroup = (props: ILxfCheckboxGroupProps<any>) => {
    const { options = [], className, ...rest } = props;
    return (
        <CheckboxGroup className={classNames(baseCls, className)} {...rest}>{
            options.map(opt =>
                <LxfCheckbox value={opt.value} label={opt.label} />
            )}
        </CheckboxGroup>
    )
}

export default LxfCheckboxGroup;
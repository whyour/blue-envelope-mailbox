import './index.scss';

import classNames from 'classnames';
import { Image, View } from '@tarojs/components';

import PngLogo from '@/assets/envelope/logo.svg';
import Rect from '@/assets/envelope/rect.svg';
import { ELetterStatus } from '@/models/communication';

import { IBaseComponentProps } from '../types';
import Taro from '@tarojs/taro';
import { message } from '@/utils';

export type IDrawerProps = IBaseComponentProps & {
    title: string;
    date: string;
    status: string;
    url?: string[];
};

const baseCls = 'envelope';

const status2Text = {
    [ELetterStatus.Waiting]: '等待中',
    [ELetterStatus.Unread]: '尚未回信',
    [ELetterStatus.Read]: '已读',
    [ELetterStatus.Send]: '已投递'
};

const LxfEnvelope: React.FunctionComponent<IDrawerProps> = (props) => {
    const { style, title, date, status, url } = props;
    const borderStyle = { active: status === ELetterStatus.Unread };
    return (
        <View
            className={baseCls}
            style={{
                ...style
            }}
            onClick={async () => {
                if (ELetterStatus.Waiting === status) {
                    return;
                }
                const hide = await message.loading('加载中');
                Taro.previewImage({
                    current: url?.[0],
                    urls: url ?? [],
                    showmenu: true,
                    complete() {
                        hide();
                    }
                });
            }}
        >
            <View className={classNames(`${baseCls}-border-row-1`, borderStyle)} />
            <View className={classNames(`${baseCls}-border-row-2`, borderStyle)} />
            <View className={classNames(`${baseCls}-border-col-1`, borderStyle)} />
            <View className={classNames(`${baseCls}-border-col-2`, borderStyle)} />
            <View className={`${baseCls}-content`}>
                <View className={`${baseCls}-top`}>
                    <View
                        className={classNames(`${baseCls}-title`, {
                            disabled: status === ELetterStatus.Waiting
                        })}
                    >
                        {title}
                    </View>
                    <View className={`${baseCls}-date`}>{date}</View>
                </View>
                <View className={`${baseCls}-footer`}>
                    <View className={classNames(`${baseCls}-footer-status`, borderStyle)}>
                        {status2Text[status]}
                    </View>
                    <View className={`${baseCls}-footer-img`}>
                        <Image className={`${baseCls}-footer-rect`} src={Rect} />
                        {status !== ELetterStatus.Waiting && (
                            <Image className={`${baseCls}-footer-logo`} src={PngLogo} />
                        )}
                    </View>
                </View>
            </View>
        </View>
    );
};

export default LxfEnvelope;

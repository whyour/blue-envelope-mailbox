import { action, computed, observable } from "mobx";
import api from "@/api";

interface ITemplateIds {
    once: string[];
    permanent: string[];
}

export class GlobalModel {
    /** 通用信息 */
    @observable private _generalInfo: any;
    /** 系统信息 */
    @observable private _systemInfo: any;
    /** 订阅模板 */
    @observable private _templateIdInfo: ITemplateIds;

    @action
    setMessageTemplate(info: ITemplateIds) {
        this._templateIdInfo = info;
    }

    @computed
    get messageTemplate() {
        return this._templateIdInfo;
    }

    @action
    setGeneralInfo(info: any) {
        this._generalInfo = info;
    }

    @computed
    get generalInfo() {
        return this._generalInfo;
    }

    @action
    setSystemInfo(info: any) {
        this._systemInfo = info;
    }

    @computed
    get systemInfo() {
        return this._systemInfo;
    }

    @action
    getMessageTemplate = async () => {
        const res = await api.lxf.getMessageTemplate();
        if (res.data.length) {
            const [...others] = res.data;
            this.setMessageTemplate({ once: [], permanent: others });
        }
    };
}

const globalModel = new GlobalModel();

export default globalModel;

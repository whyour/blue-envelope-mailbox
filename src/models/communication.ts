import { action, computed, observable } from "mobx";

import api from "@/api";
import { createUserRouterError, EUserErrorType } from "@/utils";

export enum ECommunicationStatus {
    Connecting = "1",
    ConnectStop = "0",
}

export interface ICommunicationInfo {
    id: number;
    child_name: number; //孩子姓名
    child_applicat_img: string; //孩子报名表
    status: ECommunicationStatus; //通信状态：1通信中，0通信结束
    letter_count: number; //通信次数
    letter_v_count: number; //寄信次数
    letterReason: string; //写信原因
    family_condition: string; //父母外出情况
    grade_class: string; //年级班级
    school_name: string; //学校名称
}

export enum ELetterSender {
    Child = "child",
    Volunteer = "volunteer",
}

export enum ELetterStatus {
    Waiting = "waiting",
    Read = "read",
    Unread = "unRead",
    Send = "send",
}

export interface ICommunicationLetterItem {
    id: string;
    sender: ELetterSender; //发信方
    remark: string; // 打卡记录
    status: ELetterStatus; //信件状态
    url: string[]; //信件图片地址
    create_date: number; //发信时间（秒级时间戳）
}

export interface ICommunicationLetterInfo {
    letter_list: ICommunicationLetterItem[];
}

export class CommunicationModel {
    /** 通信信息 */
    @observable.shallow private _communicationInfo: ICommunicationInfo | null;
    /** 来信信息 */
    @observable.shallow
    private _communicationLetterInfo: ICommunicationLetterInfo | null;

    @action
    cleanCommunication() {
        this._communicationInfo = null;
        this._communicationLetterInfo = null;
    }

    @action
    setCommunicationInfo(info: ICommunicationInfo) {
        this._communicationInfo = info;
    }

    @action
    setCommunicationLetterInfo(info: ICommunicationLetterInfo) {
        this._communicationLetterInfo = info;
    }

    @computed
    get communicationInfo() {
        return this._communicationInfo;
    }

    @computed
    get communicationLetterInfo() {
        return this._communicationLetterInfo;
    }

    @action
    async getCommunicationInfo() {
        try {
            // 获取通信信息
            const communicationRes = await api.lxf.getCommunicationInfo();
            if (
                !communicationRes ||
                !communicationRes.data ||
                !communicationRes.data.length
            ) {
                return;
            }
            const cmmn = communicationRes.data.find(
                (c) => c.status === ECommunicationStatus.Connecting
            );
            if (!cmmn) {
                return;
            }
            console.log("cmmn", cmmn);
            this.setCommunicationInfo(cmmn);
            const lettersRes = await api.lxf.getCommunicationLetter({
                cmmn_id: cmmn.id,
            });
            const letter_list = lettersRes?.letter_list;
            if (Array.isArray(letter_list)) {
                let idx = 0;
                while (idx < letter_list.length && idx < 9999) {
                    const letter = letter_list?.[idx];
                    if (letter.sender === ELetterSender.Child) {
                        letter.status = ELetterStatus.Unread;
                    } else {
                        break;
                    }
                    idx++;
                }
                while (idx < letter_list.length && idx < 9999) {
                    const letter = letter_list?.[idx];
                    if (letter.sender === ELetterSender.Child) {
                        letter.status = ELetterStatus.Read;
                    } else {
                        letter.status = ELetterStatus.Send;
                    }
                    idx++;
                }
            }
            this.setCommunicationLetterInfo(lettersRes);
        } catch (err) {
            console.error("err-----", err);
            throw createUserRouterError(err?.errMsg, EUserErrorType.unknown);
        }
    }
}

const communicationModel = new CommunicationModel();

export default communicationModel;

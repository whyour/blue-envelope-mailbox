import { action, computed, observable } from "mobx";

import api from "@/api";
import { createUserRouterError, EUserErrorType } from "@/utils";

export interface IVideoItem {
    id: number;
    url: string; //视频链接
    cover: string; //视频封面
    title: string; //视频标题
}

export type IFromType =
    | "radio"
    | "text"
    | "date"
    | "city"
    | "description"
    | "textarea"
    | "select"
    | "search:university"
    | "picture"
    | "pictures"; //类型

export interface IFormItem {
    name: string; //提交字段名
    title: string; //题目
    value: string; //题目回答
    required: number; //是否必填 1: 必填 0: 非必填
    type: IFromType;
    options: string[] | { [key: string]: string }; //选项
    skip_rule: { show: { [key: string]: string } }; //跳题规则（若为null，则固定）
    show: boolean;
    other_limit?: string; //其他限制条件(textarea)
}

export interface IQuestionItem {
    id: number;
    name: string; //提交字段名
    title: string; //题目
    value: string; //上一次题目回答
    type: IFromType;
    options: string[]; //选项
    required: number; //是否必填 1: 必填 0: 非必填
    other_limit?: string; //其他限制条件(textarea)
}

export interface IApplyInfo {
    video_list: IVideoItem[];
    form_description: string; //表单题目描述(富文本)
    form_item_list: IFormItem[];
    question_list: IQuestionItem[];
    question_description: string; //题库题目描述(富文本)
}

export class ApplyModel {
    @observable.shallow private _step1ApplyInfo: IApplyInfo | null;
    @observable.shallow private _step2ApplyInfo: IApplyInfo | null;
    @observable.shallow private _step3ApplyInfo: IApplyInfo | null;

    @action
    cleanApply() {
        this._step1ApplyInfo = null;
        this._step2ApplyInfo = null;
        this._step3ApplyInfo = null;
    }

    @action
    setStep1ApplyInfo(info: IApplyInfo) {
        this._step1ApplyInfo = info;
    }

    @action
    setStep2ApplyInfo(info: IApplyInfo) {
        this._step2ApplyInfo = info;
    }

    @action
    setStep3ApplyInfo(info: IApplyInfo) {
        this._step3ApplyInfo = info;
    }

    @computed
    get step1ApplyInfo() {
        return this._step1ApplyInfo;
    }

    @computed
    get step2ApplyInfo() {
        return this._step2ApplyInfo;
    }

    @computed
    get step3ApplyInfo() {
        return this._step3ApplyInfo;
    }

    @action
    async getStep1ApplyInfo() {
        try {
            const res = await api.lxf.getVolunteerApplyStep1Questions();
            this.setStep1ApplyInfo(res);
        } catch (err) {
            console.error(err);
            throw createUserRouterError(err?.errMsg, EUserErrorType.unknown);
        }
    }
    @action
    async getStep2ApplyInfo() {
        try {
            const res = await api.lxf.getVolunteerApplyStep2Questions();
            this.setStep2ApplyInfo(res);
        } catch (err) {
            console.error(err);
            throw createUserRouterError(err?.errMsg, EUserErrorType.unknown);
        }
    }
    @action
    async getStep3ApplyInfo() {
        try {
            const res = await api.lxf.getVolunteerApplyStep3Questions();
            this.setStep3ApplyInfo(res);
        } catch (err) {
            console.error(err);
            throw createUserRouterError(err?.errMsg, EUserErrorType.unknown);
        }
    }
}

const applyModel = new ApplyModel();

export default applyModel;

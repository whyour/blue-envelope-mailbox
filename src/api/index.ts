import {
    request,
    IRequestOptions,
    IRequestConfig,
} from '@/utils/request';

import { globalConfig } from '@/constants';
import LxfService from '@/service';

/**
 * 通用请求
 * @param config 请求配置
 * @param options 自定义配置
 * @returns
 */
function commonRequest<R>(config: IRequestConfig, options?: IRequestOptions) {
    const host = globalConfig.host;

    let fullUrl = config.url.includes('html') || config.url.startsWith('/api/') ? `${host}${config.url}` : `${host}${globalConfig.base_url}${config.url}`;
    const Authorization = globalConfig.sid;
    const header = Authorization ? { Authorization, ...options?.header } : options?.header

    return request<R>(
        {
            ...config,
            url: fullUrl,
        },
        {
            ...options,
            header
        },
    );
}

const api = {
    lxf: new LxfService<IRequestOptions>({
        request: commonRequest,
    }),
};

export default api;

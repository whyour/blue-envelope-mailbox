import Taro from '@tarojs/taro';
import { sleep } from './timer';

const hideLoading = () => {
    Taro.hideLoading();
};

const loading = async function (title: string) {
    await Taro.showLoading({
        title
    });

    return hideLoading;
};

const success = function (title: string, duration?: number, showIcon?: boolean) {
    return new Promise<void>((resolve) => {
        Taro.showToast({
            icon: showIcon ?? true ? 'success' : 'none',
            title,
            duration,
            complete: async () => {
                await sleep(duration ?? 0);
                resolve();
            }
        });
    });
};

const error = function (title: string, duration: number = 1500) {
    return new Promise<void>((resolve) => {
        Taro.showToast({
            icon: 'none',
            title,
            duration,
            complete: async () => {
                await sleep(duration ?? 1500);
                resolve();
            }
        });
    });
};

const message = {
    loading,
    hideLoading,
    success,
    error
};

export default message;

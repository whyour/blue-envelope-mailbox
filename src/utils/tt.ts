import { STORAGE_AWEME_INFO_KEY, STORAGE_SYSTEM_INFO_KEY } from "@/constants";
import Taro from "@tarojs/taro";
import message from "./message";

/**
 * 获取登录凭证
 * @returns
 */
export function getLoginCode() {
  return new Promise<string>((resolve, reject) => {
    tt.login({
      success: ({ code }) => {
        if (!!code) {
          resolve(code);
        } else {
          reject();
        }
      },
      fail: err => {
        reject(err);
      },
    });
  });
}
/**
 * 用户的登录态具有时效性，随着用户未使用小程序的时间增加，用户的登录态越容易失效；反之，则用户登录态可持续保持有效。
 * 使用该 API 可检查用户当前的 session 状态是否有效，登录态过期后开发者可以再调用 tt.login 获取新的用户登录态。
 * @returns
 */
function checkSession() {
  return new Promise<string | void>((resolve, reject) => {
    tt.checkSession({
      success() {
        resolve();
      },
      fail() {
        getLoginCode()
          .then((code) => resolve(code))
          .catch(reject);
      },
    });
  });
}

/**
 * 获取宿主信息
 * @returns
 */
export async function getAwemeInfoWithStorage() {
  await checkSession();

  const ret = await new Promise<any>((resolve, reject) => {
    const data = Taro.getStorageSync(STORAGE_AWEME_INFO_KEY);
    if (data) {
      return resolve(data);
    }

    tt.getUserInfo({
      withCredentials: true,
      success(res) {
        const { userInfo } = res;
        Taro.setStorageSync(STORAGE_AWEME_INFO_KEY, userInfo);
        resolve(userInfo);
      },
      fail(err) {
        console.error(err);
        message
          .error(
            err?.errNo === 10200 ? '请开启「用户信息」开关' : err.errMsg,
            1500,
          )
          .then(() => {
            err?.errNo === 10200 && tt.openSetting({});
            reject(err);
          });
      },
    });
  });
  return ret;
}


/**
 * 获取系统信息
 * @returns
 */
export async function getSystemInfoWithStorage() {
  const ret = await new Promise<any>((resolve, reject) => {
    const data = Taro.getStorageSync(STORAGE_SYSTEM_INFO_KEY);
    if (data) {
      return resolve(data);
    }

    // @ts-ignore
    tt.getSystemInfo({
      success(res) {
        console.info(res);

        Taro.setStorageSync(STORAGE_SYSTEM_INFO_KEY, res);
        resolve(res);
      },
      fail(err) {
        console.error(err);
        message
          .error(
            err?.errNo === 10200 ? '请开启「系统信息」开关' : err?.errMsg,
            1500,
          )
          .then(() => {
            err?.errNo === 10200 && tt.openSetting({});
            reject(err);
          });
      },
    });
  });
  console.log('系统信息------', ret)
  return ret;
}


export function stringRandom(length = 8) {
    const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    let chars = letters;
    let result = "";

    while (length > 0) {
        length -= 1;
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
}

type EnumType = { [s: number]: string };

export function mapEnum(
    enumerable: EnumType,
    fn: Function,
    options?: { excludes?: (string | number)[] }
): any[] {
    // 获取所有枚举成员
    const enumMembers: any[] = Object.values(enumerable);
    // 获取数值型
    const enumValues: number[] = enumMembers.filter(v => typeof v === "number");
    return enumValues.length > 0
        ? enumValues.filter(i => !options?.excludes?.includes(i)).map(v => fn(v))
        : enumMembers.filter(i => !options?.excludes?.includes(i)).map(v => fn(v));
}

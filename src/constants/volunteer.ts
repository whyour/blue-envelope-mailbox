export enum EVolunteerStatus {
    DISABLED = '0',  //禁用
    IN_APPLICATION = '1', //申请中
    NORMAL = '2', //正常
    DISUSE = '3' //淘汰
}
export enum EApplicatStatus {
    STEP_1_IN_APPLICATION = '1_0',
    STEP_1_PASSED = '1_1',
    STEP_1_RETURN = '1_2',
    STEP_2_IN_APPLICATION = '2_0',
    STEP_2_PASSED = '2_1',
    STEP_2_RETURN = '2_2',
    STEP_3_IN_APPLICATION = '3_0',
    STEP_3_PASSED = '3_1',
    STEP_3_RETURN = '3_2'
}

export enum EApplyStatus {
    CAMPUS = 1, //在校大使
    CORPORATE = 2 //在职大使
}

export enum EGender {
    BOY = 1, //男
    GIRL = 2 //女
}

export enum EFavourKey {
    LEARNING = "0", //学习方面
    RELATIONSHIP = '1', //关系处理方面
    CAREER = '2', // 职业方面
    LIFE = '3' //生活方面
}

export enum EFavourValue {
    MAJOR = '1', //主科（语文、数学、英语）
    LIBERAL_ARTS = '2', //文科（历史、地理、政治）
    SCIENCE = '3', //理科（生物、物理、化学）
    FRIEND = '4', // 朋友关系
    FAMILY = '5', //家庭关系
    SELF = '6', //自我相处（心情/情绪/心理）
    SOCIAL = '7', //社交类
    CREATIVE = '8', //创作类
    TECH = '9', //技术研究类
    SPORTS = '10', //运动类
    ART = '11', //艺术类
    LEISURE = '12' //休闲类
}

export enum EIdType {
    JUNIOR_COLLEGE_STUDENT = 1,
    UNDERGRADUATE_STUDENTS = 2,
    MASTERS = 3,
    DOCTORS = 4,
    HIGH_SCHOOL_STUDENTS = 5
}

export { default as globalConfig } from "./config";

export const STORAGE_AWEME_INFO_KEY = "STORAGE_AWEME_INFO";
export const STORAGE_SYSTEM_INFO_KEY = "STORAGE_SYSTEM_INFO";
export const STORAGE_GENERAL_INFO_KEY = "GENERAL_SYSTEM_INFO";
export const STORAGE_DISUSE_INFO_KEY = "STORAGE_DISUSE_INFO";
export const APP_SECRET = "AppSecret96a8b44fa3de65fcf38e40b9c18be6dd693f397f";
export const MATCHABLE_MONTHS = [3, 4, 5, 9, 10, 11];
export const UNMATCHED_MONTHS = [6, 7, 8, 12, 1, 2];

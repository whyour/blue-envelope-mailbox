const config = {
    /** 线上请求域名 */
    // host: 'https://poster.lanxinfeng.org',
    /** 测试环境 */
    host: 'https://lxf.t4tstudio.com',
    /** 线上请求路径前缀 */
    base_url: '/DouyinApi',
    sid: ''
};

export default config;

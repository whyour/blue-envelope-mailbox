import { ECommunicationStatus, ICommunicationInfo } from '@/models/communication';
import { University } from '@/pages/signup/components/school';

export interface IUniversityRes {
    [key: string]: University;
}

export interface IUniversityReq {
    page: number;
    per_page: number;
    keyword?: string;
}

export interface ICommunicationReq {
    page?: number;
    per_page?: number;
    status?: ECommunicationStatus;
}

export interface ICommunicationInfoRes {
    data: ICommunicationInfo[];
}

export interface ICommunicationLetterReq {
    page?: number;
    per_page?: number;
    cmmn_id: number; //通信id
}

export interface IUploadReq {
    type: string;
    title: string;
    vendor_type: string;
    file_type: string;
}

export interface IUploadRes {
    url: string;
    params: {
        'Content-Type': string;
        key: string;
        name: string;
        policy: string;
        'x-tos-algorithm': string;
        'x-tos-callback': string;
        'x-tos-callback-var': string;
        'x-tos-credential': string;
        'x-tos-date': string;
        'x-tos-signature': string;
    };
    vendor_type: string;
}

export interface IUploadOssReq {
    file: string;
}

export interface IUploadOssRes {
    file_id: number;
    file_url: string;
    status: 1;
}

export interface IWriteLetterReq {
    cmmn_id: number; //通信id
    letter_ids: string; //上传的信件图片id，多张以逗号','分隔
    remark?: string; //打卡记录
}

export interface IWriteLetterRes {
    info: 'ok' | 'fail';
    status: 1 | 0;
    event_id: string; //写信id
    charity_hours: string; //获取的志愿时
}

export interface IApplyReq {
    [key: string]: string;
}

export interface IApplyResp {
    info: 'ok' | 'fail';
    status: 1 | 0;
    score: number;
    pass_score: number;
    all_score: number;
    applicat_status: '1' | '0';
}

export interface IMessageTemplateResp {
    info: 'ok' | 'fail';
    status: 1 | 0;
    data: string[];
}

export interface IUdeskHelpResp {
    info: 'ok' | 'fail';
    status: 1 | 0;
    url: string;
}

export type TChooseFileInfo = {
    originUrl: string;
    hide?: () => void;
};

export type TUploadFileInfo = IUploadOssRes & TChooseFileInfo;

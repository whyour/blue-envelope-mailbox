import { PropsWithChildren } from 'react';
import { useError, useLaunch, useUnhandledRejection } from '@tarojs/taro';
import 'taro-ui/dist/style/components/image-picker.scss';
import 'taro-ui/dist/style/components/icon.scss';
import 'taro-ui/dist/style/components/input.scss';
import 'taro-ui/dist/style/components/search-bar.scss';
import 'taro-ui/dist/style/components/button.scss';
import 'taro-ui/dist/style/components/tab-bar.scss';
import 'taro-ui/dist/style/components/badge.scss';
import '@tarojs/taro/html5.css';
import './app.scss';
import Taro from '@tarojs/taro';

function App({ children }: PropsWithChildren) {
    useUnhandledRejection(({ reason, promise }) => {
        console.error(reason, promise);
    });

    useError((error) => {
        console.error(error);
    });

    useLaunch(() => {
        console.log('App launched.');
        (Taro as any).options.html.transformElement = (el) => {
            if (el.props.class == 'h5-a') {
                el.__handlers.tap = [
                    () => {
                        Taro.setClipboardData({
                            data: el.props.href,
                            complete(res) {
                                console.log('复制链接', res);
                            }
                        });
                    }
                ];
            }
            return el;
        };
    });

    return children;
}

export default App;

import './index.scss';

import { View, Image, Text } from '@tarojs/components';
import IconEmojisad from '@/assets/images/index/emojisad.png';
import { STORAGE_DISUSE_INFO_KEY, STORAGE_GENERAL_INFO_KEY } from '@/constants';
import Taro, { useDidShow } from '@tarojs/taro';

const baseCls = 'disuse';

const Disuse = () => {
    const state = Taro.getStorageSync(STORAGE_DISUSE_INFO_KEY) as
        | 'eliminated-user'
        | 'disabled-user'
        | 'returned-user';
    const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};

    useDidShow(() => {
        try {
            Taro.hideHomeButton();
        } catch (error) {}
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
            ...data,
            reenroll: false
        });
    });

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <Image className={`${baseCls}-wrapper-icon`} src={IconEmojisad} />
                <View className={`${baseCls}-wrapper-text`}>
                    <Text>感谢您对乡村儿童笔友书信项目的支持，很遗憾您未通过预备通信大使审核</Text>
                    {state !== 'disabled-user' && <Text>，详情可查阅【蓝信封邮筒】订阅号/短信消息</Text>}
                </View>
                {/* {state === "returned-user" && (
                    <LxfButton
                        className={`${baseCls}-wrapper-btn`}
                        title="重新报名"
                        onClick={() => {
                            Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
                                ...data,
                                reenroll: true,
                            });
                            goToPage("/pages/index/index", {
                                method: "redirectTo",
                            });
                        }}
                    />
                )} */}
            </View>
        </View>
    );
};
export default Disuse;

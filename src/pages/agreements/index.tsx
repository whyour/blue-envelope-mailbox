import React, { FunctionComponent } from 'react';
import { RichText, View } from '@tarojs/components';

import './index.scss';
import { htmlStr } from './util';

const baseCls = 'agreements';

const Certificate: FunctionComponent<{}> = () => {
    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <RichText nodes={htmlStr} />
            </View>
        </View>
    );
};

export default Certificate;

import './index.scss';

import classNames from 'classnames';
import React, { FunctionComponent, useRef, useState } from 'react';
import { View, Image } from '@tarojs/components';

import api from '@/api';
import IconAirplane from '@/assets/icons/airplane.svg';
import { LxfButton } from '@/components';
import { TChooseFileInfo, TUploadFileInfo } from '@/service/type';
import { useCommunication, useGlobal } from '@/hooks';

import LetterCard from '../components/letter-card';
import UploadCard from '../components/upload-card';
import { IFloatWrapperRef } from '@/components/lxf-float-wrapper';
import { LxfConfirmModal } from '@/components/lxf-confirm-model';
import IconCamera from '@/assets/icons/camera.svg';
import IconPhoto from '@/assets/icons/photo.svg';
import IconDivider from '@/assets/icons/divider.svg';
import { goToPage, message } from '@/utils';
import { WrapModel } from '@/models';
import Taro from '@tarojs/taro';
import { toJS } from 'mobx';
import { STORAGE_GENERAL_INFO_KEY } from '@/constants';

const baseCls = 'write-letter';

const WriteLetter: FunctionComponent<{}> = () => {
    const [imageList, setImageList] = useState<TChooseFileInfo[]>([]);
    const [additionList, setAdditionList] = useState<TChooseFileInfo[]>([]);
    const [{ communicationInfo }] = useCommunication();
    const sendLetterConfirmRef = useRef<IFloatWrapperRef>(null);
    const subRef = useRef<IFloatWrapperRef>(null);
    const [{ messageTemplate }] = useGlobal();

    const deleteLetter = (fileUrl: string) => {
        setImageList((prev) => prev.filter((image) => image.originUrl !== fileUrl));
    };

    const deleteAddition = (fileUrl: string) => {
        setAdditionList((prev) => prev.filter((image) => image.originUrl !== fileUrl));
    };

    const sendLetter = async () => {
        if (communicationInfo?.id) {
            const images = [...imageList, ...additionList];
            const hide = await message.loading('投递中');
            const result = await Promise.all(
                images.map((f) =>
                    api.lxf.uploadOss({
                        file: f.originUrl
                    })
                )
            );
            const params = {
                cmmn_id: communicationInfo.id,
                letter_ids: result.map((image) => image.file_id).join(','),
                remark: '打卡记录'
            };
            const res = await api.lxf.writeLetter(params);
            if (res.status === 1) {
                const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
                if (!data.subscribed) {
                    subRef.current?.open();
                } else {
                    goToPage('/pages/mailbox/index', { method: 'redirectTo' });
                }
            } else {
                message.error(res?.info);
            }
            setTimeout(() => {
                hide();
            });
        }
    };

    return (
        <View className={baseCls}>
            <View
                className={classNames(`${baseCls}-wrapper`, {
                    empty: imageList.length === 0
                })}
            >
                <View
                    className={classNames('image-wrapper', {
                        empty: imageList.length === 0
                    })}
                >
                    <UploadCard
                        icon={IconCamera}
                        sourceType={['album', 'camera']}
                        empty={!imageList.length}
                        chooseImageProps={{
                            count: 20
                        }}
                        onChoose={(images: TChooseFileInfo[]) => {
                            setImageList((prev) => prev.concat(...images));
                        }}
                    />
                    {imageList.length > 0 &&
                        imageList.map((image, index) => (
                            <LetterCard
                                key={image.originUrl}
                                letterInfo={image}
                                id={index + 1}
                                onDelete={deleteLetter}
                                onClick={async () => {
                                    const hide = await message.loading('加载中');
                                    Taro.previewImage({
                                        current: imageList[index].originUrl,
                                        urls: imageList.map((x) => x.originUrl),
                                        showmenu: true,
                                        complete() {
                                            hide();
                                        }
                                    });
                                }}
                            />
                        ))}
                    {imageList.length === 0 && (
                        <View>
                            <View className={`${baseCls}-title`}>
                                拍摄信件
                                <View
                                    style={{
                                        color: '#FE2C55',
                                        display: 'inline'
                                    }}
                                >
                                    *
                                </View>
                            </View>
                            <View className={`${baseCls}-content`}>逐页拍摄你的手写信件</View>
                        </View>
                    )}
                </View>
                <Image src={IconDivider} style={{ height: 1 }} />
                <View
                    className={classNames('image-wrapper', {
                        empty: additionList.length === 0
                    })}
                >
                    {additionList.length < 2 && (
                        <UploadCard
                            icon={IconPhoto}
                            sourceType={['album']}
                            empty={!additionList.length}
                            chooseImageProps={{
                                count: 2 - additionList.length
                            }}
                            onChoose={(images: TUploadFileInfo[]) =>
                                setAdditionList((prev) => prev.concat(...images))
                            }
                        />
                    )}
                    {additionList.length > 0 &&
                        additionList.map((image, index) => (
                            <LetterCard
                                key={image.originUrl}
                                letterInfo={image}
                                id={index + 1}
                                onDelete={deleteAddition}
                                onClick={async () => {
                                    const hide = await message.loading('加载中');
                                    Taro.previewImage({
                                        current: additionList[index].originUrl,
                                        urls: additionList.map((x) => x.originUrl),
                                        showmenu: true,
                                        complete() {
                                            hide();
                                        }
                                    });
                                }}
                            />
                        ))}
                    {additionList.length === 0 && (
                        <View>
                            <View className={`${baseCls}-title`}>
                                上传附件
                                <View style={{ fontSize: 14 }}>（仅限两张日常照片）</View>
                            </View>
                            <View className={`${baseCls}-content`}>日常照片将会在彩色打印后邮寄给孩子</View>
                        </View>
                    )}
                </View>
                <View className={`${baseCls}-sendbtn`}>
                    <LxfButton
                        title={'投递'}
                        icon={IconAirplane}
                        disabled={!imageList.length}
                        onClick={() => sendLetterConfirmRef.current?.open()}
                    />
                </View>
            </View>
            <LxfConfirmModal
                ref={sendLetterConfirmRef}
                title={'确定投递你的信件？'}
                showCancelBtn
                showOkBtn
                okText={'投递'}
                onOk={sendLetter}
                componentProps={{
                    content: '请先按照顺序阅读你上传的信件，一旦投递，信件将不可撤回。'
                }}
            />
            <LxfConfirmModal
                ref={subRef}
                title="订阅后续来信通知"
                footer={
                    <View className={`common-modal-bottom`}>
                        <View
                            className={`common-modal-btn`}
                            onClick={() => {
                                subRef.current?.close();
                                goToPage('/pages/mailbox/index', { method: 'redirectTo' });
                            }}
                        >
                            暂不订阅
                        </View>
                        <View
                            className={classNames(`common-modal-btn`, {
                                primary: true
                            })}
                            onClick={() => {
                                Taro.requestSubscribeMessage({
                                    tmplIds: toJS(messageTemplate).permanent,
                                    success() {
                                        message.success('订阅成功');
                                        const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
                                        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
                                            ...data,
                                            subscribed: true
                                        });
                                        subRef.current?.close();
                                        goToPage('/pages/mailbox/index', { method: 'redirectTo' });
                                    },
                                    fail(res: any) {
                                        if (res.errNo === 21109 || res.errNo === 21108) {
                                            message.error(`请前往小程序设置打开订阅开关`);
                                        } else {
                                            message.error(`订阅失败 ${res.errMsg}`);
                                        }
                                    }
                                });
                            }}
                        >
                            订阅通知
                        </View>
                    </View>
                }
            />
        </View>
    );
};

export default WrapModel(WriteLetter);

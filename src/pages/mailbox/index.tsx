import './index.scss';

import dayjs from 'dayjs';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Image, Text, View } from '@tarojs/components';

import IconArrowRight from '@/assets/icons/arrow-right.svg';
import IconWrite from '@/assets/icons/write.svg';
import IconBlackMy from '@/assets/images/footer/my-black.png';
import IconBlackMyActive from '@/assets/images/footer/my-active-black.png';
import IconBlackMail from '@/assets/images/footer/mailbox-black.png';
import IconBlackMailActive from '@/assets/images/footer/mailbox-active-black.png';
import noCommunicate from '@/assets/images/index/nocommunicate.gif';
import { LxfButton, LxfEnvelope } from '@/components';
import { useCommunication, withCommunication } from '@/hooks';
import { ELetterSender, ELetterStatus, ICommunicationLetterItem } from '@/models/communication';
import { goToPage, message } from '@/utils';
import { IFloatWrapperRef } from '../login';
import { ChildInfoDrawer } from './components/child-info-drawer';
import { MATCHABLE_MONTHS, UNMATCHED_MONTHS } from '@/constants';
import { useDidShow, usePageScroll } from '@tarojs/taro';
import Taro from '@tarojs/taro';
import classNames from 'classnames';

const baseCls = 'mailbox-page';

const MailBox = () => {
    const childInfoRef = useRef<IFloatWrapperRef>(null);
    const [image, setImage] = useState<string>();
    const [{ communicationInfo, communicationLetterInfo }] = useCommunication();
    const letter_list = communicationLetterInfo?.letter_list ?? [];
    const object = communicationInfo ? `通信对象：${communicationInfo?.child_name ?? '获取失败'}` : '';

    useDidShow(() => {
        if (communicationInfo) {
            useCommunication()[0].getCommunicationInfo();
        }
        try {
            Taro.hideHomeButton();
        } catch (error) {}
    });

    const isDark = dayjs().hour() >= 20 || dayjs().hour() <= 6;

    useEffect(() => {
        const run = async () => {
            const hide = await message.loading('加载中');
            if (isDark) {
                Taro.setTabBarStyle({
                    backgroundColor: '#323030',
                    color: '#ffffff',
                    borderStyle: 'black',
                    selectedColor: '#ffffff'
                });
                Taro.setTabBarItem({
                    index: 0,
                    iconPath: IconBlackMail,
                    selectedIconPath: IconBlackMailActive
                });
                Taro.setTabBarItem({ index: 1, iconPath: IconBlackMy, selectedIconPath: IconBlackMyActive });
            }
            Taro.getImageInfo({
                src: `https://douyin-miniapp.tos-cn-guangzhou.volces.com/bg${isDark ? 2 : 1}.png`,
                success(result) {
                    setImage(result.path);
                },
                complete() {
                    hide();
                }
            });
        };
        run();
    }, [isDark]);

    usePageScroll((payload) => {
        if (payload.scrollTop > 10) {
            Taro.setNavigationBarColor({ backgroundColor: '#ffffff', frontColor: '#000000' });
        } else {
            Taro.setNavigationBarColor({ backgroundColor: '#000000', frontColor: '#ffffff' });
        }
    });

    const HeaderMessage = useMemo(() => {
        if (!communicationInfo) {
            const month = new Date().getMonth();
            if (UNMATCHED_MONTHS.includes(month + 1)) {
                return {
                    title: '寒暑假暂停配对',
                    desc: '三月和九月会重启配对，到时将会为你优先安排配对'
                };
            } else if (MATCHABLE_MONTHS.includes(month + 1)) {
                return {
                    title: '匹配通信对象中',
                    desc: '平均 21 个工作日内完成配对，请留意邮箱/短信信息。'
                };
            }
        }
        if (!letter_list.length) {
            return { title: '已完成配对', desc: '将在6小时内收到第一封来信' };
        }
        const sendCnt = letter_list.filter((letter) => letter.sender === ELetterSender.Volunteer).length;
        if (sendCnt > 0) {
            return { title: `已寄出 ${sendCnt || 0} 封信`, desc: '' };
        }
        return { title: '尚未寄出回信', desc: '' };
    }, [letter_list, communicationInfo]);

    return (
        <View className={baseCls}>
            <View
                className={classNames(`${baseCls}-background`, {
                    dark: isDark
                })}
                style={{ backgroundImage: `url(${image})` }}
            ></View>
            <View className={`${baseCls}-wrapper`}>
                <View className={`${baseCls}-wrapper-scroll`}>
                    <View className={`${baseCls}-top`}>
                        <View className={`${baseCls}-title`}>{HeaderMessage.title}</View>
                        {HeaderMessage.desc && (
                            <View className={`${baseCls}-desc`}>{HeaderMessage.desc}</View>
                        )}
                        {object && (
                            <View
                                className={`${baseCls}-object`}
                                onClick={() => {
                                    childInfoRef.current?.open();
                                }}
                            >
                                <Text className={`${baseCls}-object-text`}> {object} </Text>
                                {communicationInfo && (
                                    <Image
                                        className={`${baseCls}-icon-arrow-right`}
                                        src={IconArrowRight}
                                        mode="aspectFit"
                                    />
                                )}
                            </View>
                        )}
                    </View>
                    <View className={`envelope-list`}>
                        {letter_list.length ? (
                            letter_list.map((letter: ICommunicationLetterItem) => {
                                const { status, sender, create_date, url } = letter ?? {};
                                return (
                                    <LxfEnvelope
                                        status={status}
                                        title={
                                            sender === ELetterSender.Child
                                                ? `${communicationInfo?.child_name}的来信`
                                                : '我的回信'
                                        }
                                        date={dayjs(create_date * 1000).format('YYYY.MM.DD')}
                                        url={url}
                                    />
                                );
                            })
                        ) : communicationInfo ? (
                            <LxfEnvelope
                                status={ELetterStatus.Waiting}
                                title={'截至目前，尚未收到来信'}
                                date={dayjs().format('YYYY.MM.DD')}
                            />
                        ) : (
                            <View className={`${baseCls}-no-communicate`}>
                                <Image style={{ width: 361, height: 197 }} src={noCommunicate} />
                            </View>
                        )}
                    </View>
                </View>
                {Boolean(letter_list.length) ? (
                    <View
                        className={`${baseCls}-write`}
                        onClick={() => {
                            goToPage('/pages/mailbox/write/index');
                        }}
                    >
                        <Image style={{ width: 16, height: 16, marginRight: 6 }} src={IconWrite} />
                        回信
                    </View>
                ) : (
                    <LxfButton className={`${baseCls}-share-btn`} title="让更多人来参与" openType="share" />
                )}
            </View>
            <ChildInfoDrawer
                ref={childInfoRef}
                maskClosable
                height={525}
                title={communicationInfo?.child_name}
                style={{ borderRadius: '26px 26px 0 0' }}
            />
        </View>
    );
};

export default withCommunication({
    auto: true,
    inject: true
})(MailBox);

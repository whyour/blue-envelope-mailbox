import './index.scss';

import Taro from '@tarojs/taro';
import { Image, View } from '@tarojs/components';

import api from '@/api';
import classNames from 'classnames';
import { IUploadOssRes, TChooseFileInfo, TUploadFileInfo } from '@/service/type';
import { message } from '@/utils';

const baseCls = 'upload-card';

interface Props {
    onChoose?: (v: TChooseFileInfo[]) => void;
    onUpload?: (v: IUploadOssRes[]) => void;
    empty: boolean;
    sourceType: (keyof Taro.chooseImage.sourceType)[];
    icon: any;
    chooseImageProps?: Taro.chooseImage.Option;
}
const UploadCard = (props: Props) => {
    const { onUpload, onChoose, empty, sourceType, icon, chooseImageProps } = props;

    const chooseImage = () => {
        Taro.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType, // 可以指定来源是相册还是相机，默认二者都有，在H5浏览器端支持使用 `user` 和 `environment`分别指定为前后摄像头
            ...chooseImageProps,
            success: async (res) => {
                console.log('图片获取成功', res);
                const tempFilePaths = res.tempFilePaths;
                if (typeof onChoose === 'function') {
                    onChoose(tempFilePaths.map((x) => ({ originUrl: x })));
                    return;
                }
                const hide = await message.loading('上传中');
                const result: TUploadFileInfo[] = [];
                for (const f of tempFilePaths) {
                    const res = await api.lxf.uploadOss({
                        file: f
                    });
                    result.push({ ...res, originUrl: f, hide });
                }
                onUpload?.(result);
            }
        });
    };

    return (
        <>
            <View className={classNames(baseCls, { empty })} onClick={chooseImage}>
                <View className={`${baseCls}-back`} />
                <Image className={`${baseCls}-icon`} src={icon} />
            </View>
        </>
    );
};

export default UploadCard;

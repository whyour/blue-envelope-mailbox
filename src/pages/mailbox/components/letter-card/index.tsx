import './index.scss';

import { Icon, Image, View, Text } from '@tarojs/components';

import { TChooseFileInfo } from '@/service/type';
import CloseIcon from '@/assets/envelope/close.svg';

const baseCls = 'letter-card';

interface ILetterCard {
    letterInfo: TChooseFileInfo;
    id?: number;
    onDelete: (fileUrl: string) => void;
    onClick: () => void;
}

const LetterCard = (props: ILetterCard) => {
    const { letterInfo, id, onDelete, onClick } = props;
    return (
        <View className={`${baseCls}`}>
            <View className={`${baseCls}-delete`} onClick={() => onDelete(letterInfo.originUrl)}>
                <Image src={CloseIcon} />
            </View>
            <View className={`${baseCls}-back`} />
            <Image
                className={`${baseCls}-letter`}
                src={letterInfo.originUrl}
                onClick={onClick}
                mode="aspectFit"
                onLoad={() => {
                    letterInfo.hide?.();
                }}
            />
            {id ? <View className={`${baseCls}-id`}>{id}</View> : null}
        </View>
    );
};

export default LetterCard;

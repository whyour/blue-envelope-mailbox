import './index.scss'

import { FunctionComponent } from 'react'
import { Text, View } from '@tarojs/components'

import { LxfFloatWrapper } from '@/components'
import { useCommunication } from '@/hooks';
import { ICommunicationInfo } from '@/models/communication';
import classNames from 'classnames';

const baseCls = 'child_info';

const ChildInfo: FunctionComponent<{}> = () => {
  const [{ communicationInfo }] = useCommunication();
  const { letterReason, family_condition, grade_class, school_name } = communicationInfo ?? {} as ICommunicationInfo;
  return (
    <View className={baseCls}>
      <View className={`${baseCls}-desp`}>
        <Text className={`${baseCls}-desp-label`}>学校</Text>
      </View>
      <View className={`${baseCls}-info`}>
        <Text className={`${baseCls}-info-value`}> {school_name}</Text>
      </View>
      <View className={`${baseCls}-desp`}>
        <Text className={`${baseCls}-desp-label`}>年级班级</Text>
      </View>
      <View className={`${baseCls}-info`}>
        <Text className={`${baseCls}-info-value`}> {grade_class}</Text>
      </View>
      <View className={`${baseCls}-desp`}>
        <Text className={`${baseCls}-desp-label`}>父母外出情况</Text>
      </View>
      <View className={`${baseCls}-info`}>
        <Text className={`${baseCls}-info-value`}> {family_condition}</Text>
      </View>
      <View className={`${baseCls}-desp`}>
        <Text className={`${baseCls}-desp-label`}>写信原因</Text>
      </View>
      <View className={classNames(`${baseCls}-info`, `${baseCls}-info-reason`)}>
        <View className={`${baseCls}-info-value`}>{letterReason}</View>
      </View>
    </View>
  );
};

export default ChildInfo;


export const ChildInfoDrawer = LxfFloatWrapper.withDrawer<{}>()(ChildInfo);

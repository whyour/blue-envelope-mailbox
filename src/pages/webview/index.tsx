import React, { FunctionComponent } from 'react';
import { View, WebView } from '@tarojs/components';
import { withUser } from '@/hooks';
import { useRouter } from '@tarojs/taro';

import './index.scss';

const baseCls = 'webview';

const Consult: FunctionComponent<{}> = () => {
    const router = useRouter();
    console.log('链接', decodeURIComponent(router.params.url || ''));

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <WebView src={router.params.url ? decodeURIComponent(router.params.url) : ''} />
            </View>
        </View>
    );
};

export default withUser({
    auto: true,
    inject: false
})(Consult);

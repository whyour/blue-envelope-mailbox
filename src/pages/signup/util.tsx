import {
    LxfCheckboxGroup,
    LxfInput,
    LxfRadioGroup,
    LxfTextarea,
} from "@/components";
import { IFromType } from "@/models/apply";
import CityComp from "./components/city";
import DateComp from "./components/date";
import UniversityComp from "./components/school";
import ImagePickerComp from "./components/image";

export const form2Comp = (type: IFromType) => {
    const map = {
        city: CityComp,
        select: LxfRadioGroup,
        textarea: LxfTextarea,
        text: LxfInput,
        radio: LxfRadioGroup,
        checkbox: LxfCheckboxGroup,
        date: DateComp,
        "search:university": UniversityComp,
        picture: ImagePickerComp,
        pictures: ImagePickerComp,
    };
    return map[type] || null;
};

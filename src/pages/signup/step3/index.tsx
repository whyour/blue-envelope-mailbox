import "./index.scss";

import classNames from "classnames";
import { useEffect, useMemo, useRef, useState } from "react";
import { Form, View } from "@tarojs/components";

import { useApply, withUser } from "@/hooks";
import { goToPage, message } from "@/utils";
import { LxfButton, LxfCard } from "@/components";
import api from "@/api";
import { IApplyInfo } from "@/models/apply";
import { form2Comp } from "../util";
import { LxfConfirmModal } from "@/components/lxf-confirm-model";
import { IFloatWrapperRef } from "@/components/lxf-float-wrapper";
import { STORAGE_GENERAL_INFO_KEY } from "@/constants";
import Taro from "@tarojs/taro";
const baseCls = "signup-page-step3";

const SignUp = () => {
    const signRef = useRef<IFloatWrapperRef>(null);
    const errorRef = useRef<IFloatWrapperRef>(null);
    const [formValues, setFormValues] = useState<any>();
    const [apply] = useApply();
    const { step3ApplyInfo = {} } = apply ?? {};
    const { form_item_list, question_list, form_description } =
        step3ApplyInfo as IApplyInfo;

    const list = useMemo(() => {
        return [...(question_list || []), ...(form_item_list || [])];
    }, [question_list, form_item_list]);

    const handleConfirm = async () => {
        const info = formValues;
        const params = { ...info };
        const hide = await message.loading('提交中');
        const res = await api.lxf.postVolunteerApplyStep3Answer(params);
        hide();
        if (res.status === 1) {
            if (res.applicat_status === "1") {
                goToPage("/pages/mailbox/index", { method: "redirectTo" });
            } else {
                goToPage("/pages/in-application/index", {
                    method: "redirectTo",
                });
            }
        } else {
            errorRef.current?.open({ title: res.info });
        }
    };

    const handleSignUp = (e: any) => {
        setFormValues(e.target.value);
        signRef.current?.open();
    };

    const resetData = () => {
        const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
            ...data,
            subscribed: false,
        });
    };

    useEffect(() => {
        const run = async () => {
            if (!apply.step3ApplyInfo) {
                const hide = await message.loading("加载中");
                await apply.getStep3ApplyInfo();
                hide();
            }
        };
        run();
        resetData();
    }, []);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={`${baseCls}-info`}>
                    <View className={classNames('lxf_info_text')}>
                        <View className="taro_html" dangerouslySetInnerHTML={{ __html: form_description }} />
                    </View>
                    <Form onSubmit={handleSignUp} className={`${baseCls}-info`}>
                        {list?.map((item) => {
                            if (!item) return null;
                            const {
                                title,
                                required,
                                type,
                                name,
                                other_limit,
                                options: _options,
                            } = item;
                            let limit = 0;
                            try {
                                const limits = JSON.parse(other_limit || "{}");
                                if (limits?.min_limit) {
                                    limit = parseInt(limits?.min_limit, 10);
                                }
                            } catch (error) {}
                            const options = Array.isArray(_options)
                                ? _options.map((opt) => ({
                                      label: opt,
                                      value: opt,
                                  }))
                                : typeof _options === "object"
                                ? Object.values(_options)?.map((opt) => ({
                                      label: opt,
                                      value: opt,
                                  }))
                                : [];
                            const Comp = form2Comp(type);
                            return type === "description" ? (
                                <View className={classNames("lxf_info_text")}>
                                    {title}
                                </View>
                            ) : (
                                <LxfCard
                                    title={`${title}${
                                        Number(required) === 1 ? "*" : ""
                                    }`}
                                >
                                    {Comp ? (
                                        <Comp
                                            mode={type}
                                            name={name}
                                            limit={
                                                type === "picture" ? 1 : limit
                                            }
                                            multiple={type === "pictures"}
                                            options={options}
                                            placeholder={`请输入${title}`}
                                        />
                                    ) : null}
                                </LxfCard>
                            );
                        })}

                        <View className={`${baseCls}-submit-btn`}>
                            <LxfButton title="提交" formType="submit" />
                        </View>
                    </Form>
                </View>
            </View>
            <LxfConfirmModal
                ref={signRef}
                title={"确定提交？"}
                showCancelBtn
                showOkBtn
                okText={"提交"}
                onOk={handleConfirm}
            />
            <LxfConfirmModal
                ref={errorRef}
                showOkBtn
                okText="确定"
                onOk={() => {
                    errorRef.current?.close();
                }}
            />
        </View>
    );
};
export default withUser({
    auto: true,
    inject: true,
})(SignUp);

import './index.scss';

import classNames from 'classnames';
import { useEffect } from 'react';
import { View } from '@tarojs/components';

import { useApply, withUser } from '@/hooks';
import { goToPage, message } from '@/utils';
import { LxfButton } from '@/components';
import { IApplyInfo } from '@/models/apply';
import LxfVideo from '@/components/lxf-video';
import Taro from '@tarojs/taro';
import { STORAGE_GENERAL_INFO_KEY } from '@/constants';

const baseCls = 'watch-video';

const VideoPage = () => {
    const [apply] = useApply();
    const { step2ApplyInfo = {} } = apply ?? {};
    const { video_list = [] } = step2ApplyInfo as IApplyInfo;

    useEffect(() => {
        const run = async () => {
            if (!apply.step2ApplyInfo) {
                const hide = await message.loading('加载中');
                await apply.getStep2ApplyInfo();
                hide();
            }
        };
        run();
    }, []);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={'lxf_h1_text'}>二、观看书信培训视频</View>
                <View className={`${baseCls}-info`}>
                    <View className={classNames('lxf_info_text')}>
                        欢迎您进入观看书信培训视频，请您务必认真、完整观看视频，以帮助你完成下一阶段的1-30题答题，考点将在视频的不同时间点出现，跳过视频盲填答案将错过宝贵考核机会。
                    </View>
                    <View className={'lxf_h2_text'}>课程视频</View>
                    {video_list.map((item) => {
                        if (!item) return null;
                        console.log('视频链接', item.url);
                        return <LxfVideo src={item.url} poster={item.cover} />;
                    })}
                </View>
                <View className={`${baseCls}-submit-btn`}>
                    <LxfButton
                        title="学习完毕，去考核"
                        onClick={() => {
                            const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
                            Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
                                ...data,
                                videoVisited: true
                            });
                            goToPage('/pages/signup/step2/exam/index', {
                                method: 'redirectTo'
                            });
                        }}
                    />
                </View>
            </View>
        </View>
    );
};
export default withUser({
    auto: true,
    inject: true
})(VideoPage);

import "./index.scss";

import classNames from "classnames";
import { useEffect, useMemo, useRef, useState } from "react";
import { Text, Form, RichText, View } from "@tarojs/components";

import { useApply, withUser } from "@/hooks";
import { goToPage, message } from "@/utils";
import { LxfButton, LxfCard } from "@/components";
import api from "@/api";
import { IApplyInfo } from "@/models/apply";
import { form2Comp } from "../../util";
import { LxfConfirmModal } from "@/components/lxf-confirm-model";
import { IFloatWrapperRef } from "@/components/lxf-float-wrapper";
import { IApplyResp } from "@/service/type";
import { STORAGE_GENERAL_INFO_KEY } from "@/constants";
import Taro from "@tarojs/taro";

const baseCls = "signup-page";

const SignUp = () => {
    const signRef = useRef<IFloatWrapperRef>(null);
    const passRef = useRef<IFloatWrapperRef>(null);
    const cannotPassRef = useRef<IFloatWrapperRef>(null);
    const errorRef = useRef<IFloatWrapperRef>(null);
    const [apply] = useApply();
    const { step2ApplyInfo = {} } = apply ?? {};
    const {
        form_item_list = [],
        question_list = [],
        question_description,
        form_description,
    } = step2ApplyInfo as IApplyInfo;
    const [formValues, setFormValues] = useState<any>();
    const [step2Res, setStep2Res] = useState<IApplyResp>();

    const list = useMemo(() => {
        return [...(question_list || []), ...(form_item_list || [])];
    }, [question_list, form_item_list]);

    const handleConfirm = async () => {
        const info = formValues;
        const params = { ...info };
        const hide = await message.loading('提交中');
        const res = await api.lxf.postVolunteerApplyStep2Answer(params);
        hide();
        if (res && res.status === 1) {
            if (res.all_score) {
                setStep2Res(res);
                if (res.score >= res.pass_score) {
                    passRef.current?.open();
                } else {
                    cannotPassRef.current?.open();
                }
            } else {
                if (res.applicat_status === "1") {
                    goToPage("/pages/signup/step3/index", {
                        method: "redirectTo",
                    });
                } else {
                    goToPage("/pages/in-application/index", {
                        method: "redirectTo",
                    });
                }
            }
        } else {
            errorRef.current?.open({ title: res.info });
        }
    };

    const handleSignUp = (e: any) => {
        setFormValues(e.target.value);
        signRef.current?.open();
    };

    const resetData = () => {
        const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
            ...data,
            subscribed: false,
        });
    };

    useEffect(() => {
        const run = async () => {
            if (!apply.step2ApplyInfo) {
                const hide = await message.loading("加载中");
                await apply.getStep2ApplyInfo();
                hide();
            }
        };
        run();
        resetData();
    }, []);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={"lxf_h1_text"}>三、理论考试</View>
                <View className={`${baseCls}-desc`}>
                    <View
                        className="taro_html"
                        dangerouslySetInnerHTML={{ __html: form_description }}
                    />
                    <Text
                        className="link"
                        onClick={() => {
                            goToPage("/pages/signup/step2/video/index", {
                                method: "redirectTo",
                            });
                        }}
                    >
                        返回观看培训视频
                    </Text>
                </View>
                <View className={`${baseCls}-info`}>
                    <RichText
                        className={classNames("lxf_info_text")}
                        nodes={question_description}
                    />
                    <Form onSubmit={handleSignUp} className={`${baseCls}-info`}>
                        {list?.map((item) => {
                            if (!item) return null;
                            const {
                                title,
                                required,
                                type,
                                name,
                                options: _options,
                                other_limit,
                            } = item;
                            let limit = 0;
                            try {
                                const limits = JSON.parse(other_limit || "{}");
                                if (limits?.min_limit) {
                                    limit = parseInt(limits?.min_limit, 10);
                                }
                            } catch (error) {}
                            const options = Array.isArray(_options)
                                ? _options.map((opt) => ({
                                      label: opt,
                                      value: opt,
                                  }))
                                : typeof _options === "object"
                                ? Object.values(_options)?.map((opt) => ({
                                      label: opt,
                                      value: opt,
                                  }))
                                : [];
                            const Comp = form2Comp(type);
                            return type === "description" ? (
                                <View className={classNames("lxf_info_text")}>
                                    {title}
                                </View>
                            ) : (
                                <LxfCard
                                    title={`${title}${
                                        Number(required) === 1 ? "*" : ""
                                    }`}
                                >
                                    {Comp ? (
                                        <Comp
                                            mode={type}
                                            name={name}
                                            limit={
                                                type === "picture" ? 1 : limit
                                            }
                                            multiple={type === "pictures"}
                                            options={options}
                                            placeholder={`请输入${title}`}
                                        />
                                    ) : null}
                                </LxfCard>
                            );
                        })}

                        <View className={`${baseCls}-submit-btn`}>
                            <LxfButton title="提交" formType="submit" />
                        </View>
                    </Form>
                </View>
            </View>
            <LxfConfirmModal
                ref={signRef}
                title={"确定提交答案吗？"}
                showCancelBtn
                showOkBtn
                okText={"提交"}
                onOk={handleConfirm}
            />
            <LxfConfirmModal
                ref={passRef}
                title={
                    <>
                        <View>恭喜你通过考试</View>
                        {/* <View>请耐心等待审核结果</View> */}
                    </>
                }
                showOkBtn
                okText={"我知道了"}
                componentProps={{
                    content: `你的分数为${step2Res?.score}分，高于${step2Res?.pass_score}分`,
                }}
                onOk={() =>
                    goToPage("/pages/signup/step3/index", {
                        method: "redirectTo",
                    })
                }
            />
            <LxfConfirmModal
                ref={cannotPassRef}
                title={"分数不足，未能通过考试"}
                showOkBtn
                okText={
                    <LxfButton
                        style={{ marginBottom: 20, width: "calc(100% - 40px)" }}
                        title="继续答题"
                    />
                }
                componentProps={{
                    content: `抱歉，您的分数为${step2Res?.score}分，低于${step2Res?.pass_score}份，请重新答题提交！`,
                }}
            />
            <LxfConfirmModal
                ref={errorRef}
                showOkBtn
                okText="确定"
                onOk={() => {
                    errorRef.current?.close();
                }}
            />
        </View>
    );
};
export default withUser({
    auto: true,
    inject: true,
})(SignUp);

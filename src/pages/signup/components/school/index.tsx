import "./index.scss";
import {
    Input,
    PickerView,
    PickerViewColumn,
    View,
    Image,
    ScrollView,
} from "@tarojs/components";
import { useEffect, useMemo, useState } from "react";
import api from "@/api";
import { AtSearchBar } from "taro-ui";
import IconPickerArrow from "../../../../components/lxf-picker/assest/picker-arrow.svg";
import classNames from "classnames";

const baseCls = "info-school";

export interface University {
    id: string;
    name: string;
}

const UniversityComp = (props) => {
    const { name, placeholder } = props;
    const [universityList, setUniversityList] = useState<University[]>([]);
    const [visible, setVisible] = useState<boolean>(false);
    const [pickerValue, setPickerValue] = useState<number[]>([5]);
    const [searchValue, setSearchValue] = useState<string>("");
    const [fadeOut, setFadeOut] = useState<boolean>(false);
    const [selectInfo, setSelectInfo] = useState({ id: "", name: "" });

    const fetchUniversityList = async (keyword?: string) => {
        const res = await api.lxf.getUniversity({
            keyword,
            page: 1,
            per_page: 20,
        });
        const list = Object.values(res || {}).filter((item) => item.id);
        setUniversityList(list);
        if (list.length) {
            const number = list.length > 10 ? 5 : Math.floor(list.length / 2);
            setPickerValue([number]);
        }
    };

    const onCancel = () => {
        setFadeOut(true);
        setTimeout(() => {
            setVisible(false);
            setSearchValue("");
            setFadeOut(false);
            fetchUniversityList();
        }, 350);
    };

    useEffect(() => {
        fetchUniversityList();
    }, []);

    return (
        <View className={`${baseCls}`}>
            <Input
                name={name}
                value={selectInfo?.id}
                style={{ display: "none" }}
            />
            <View
                className={`${baseCls}-origin`}
                onClick={() => {
                    setVisible(true);
                }}
            >
                <View className={`${baseCls}-value`}>
                    {selectInfo?.name || placeholder}
                </View>
                <Image className={`${baseCls}-icon`} src={IconPickerArrow} />
            </View>
            {visible && (
                <View
                    className={classNames(`${baseCls}-background`, {
                        [`${baseCls}-background-active`]: visible,
                    })}
                >
                    <View
                        className={classNames(`${baseCls}-overlay`, {
                            "fade-out": fadeOut,
                        })}
                        onClick={onCancel}
                    ></View>
                    <View
                        className={classNames(`${baseCls}-container`, {
                            "fade-out": fadeOut,
                        })}
                    >
                        <View className={`${baseCls}-confirm`}>
                            <View className="cancel" onClick={onCancel}>
                                取消
                            </View>
                            <AtSearchBar
                                value={searchValue}
                                placeholder="请输入学校名称"
                                onChange={(v) => {
                                    setSearchValue(v);
                                }}
                                onActionClick={() => {
                                    fetchUniversityList(searchValue);
                                }}
                            />
                            <View
                                className="confirm"
                                onClick={() => {
                                    setSelectInfo(universityList[pickerValue?.[0]])
                                    onCancel();
                                }}
                            >
                                确定
                            </View>
                        </View>
                        <PickerView
                            className="picker-view"
                            indicatorClass="indicator"
                            immediateChange
                            value={pickerValue}
                            onChange={(e) => {
                                setPickerValue(e.detail.value);
                            }}
                        >
                            <PickerViewColumn>
                                {universityList?.map((item, index) => {
                                    return (
                                        <View
                                            className={classNames("item", {
                                                selected:
                                                    index === pickerValue?.[0],
                                            })}
                                        >
                                            {item.name}
                                        </View>
                                    );
                                })}
                            </PickerViewColumn>
                        </PickerView>
                    </View>
                </View>
            )}
        </View>
    );
};
export default UniversityComp;

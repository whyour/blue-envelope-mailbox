import { LxfPicker } from '@/components'
import { useState } from 'react';

const DateComp = (props) => {
    const [date, setDate] = useState<string>();

    return (
        <LxfPicker {...props} mode='date' onChange={(e) => setDate(e.target.value)} placeholder={"选择你的出生日期"} >
            {date}
        </LxfPicker>
    )
}
export default DateComp;

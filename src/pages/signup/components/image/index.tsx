import "./index.scss";
import { Input, View } from "@tarojs/components";
import { useState } from "react";
import api from "@/api";
import { AtImagePicker } from "taro-ui";
import { TUploadFileInfo } from "@/service/type";
import { message } from "@/utils";
import Taro from "@tarojs/taro";

const baseCls = "info-image";

export interface University {
    id: string;
    name: string;
}

const ImagePickerComp = (props) => {
    const { name, limit, ...others } = props;
    const [files, setFiles] = useState<Array<{ url: string; id: number }>>([]);

    return (
        <View className={`${baseCls}`}>
            <Input
                name={name}
                value={files.map((x) => x.id).join(",")}
                style={{ display: "none" }}
            />
            <AtImagePicker
                {...others}
                files={files}
                count={limit}
                showAddBtn={limit === 1 && files.length === 1 ? false : true}
                onImageClick={async (index) => {
                    const hide = await message.loading("加载中");
                    Taro.previewImage({
                        current: files[index].url,
                        urls: files.map((x) => x.url),
                        showmenu: true,
                        complete() {
                            hide();
                        },
                    });
                }}
                onChange={async (
                    newFiles,
                    oType: "add" | "remove",
                    index: number
                ) => {
                    if (oType === "add") {
                        const hide = await message.loading("上传中");
                        const result: TUploadFileInfo[] = [];
                        for (const f of newFiles) {
                            const res = await api.lxf.uploadOss({
                                file: f.url,
                            });
                            result.push({ ...res, originUrl: f.url });
                        }
                        setFiles(
                            result.map((x) => ({
                                url: x.originUrl,
                                id: x.file_id,
                            }))
                        );
                        hide();
                    } else {
                        const _files = [...files];
                        _files.splice(index, 1);
                        setFiles(_files);
                    }
                }}
            />
        </View>
    );
};
export default ImagePickerComp;

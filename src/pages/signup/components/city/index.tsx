import "./index.scss";
import { LxfPicker } from "@/components";
import { Input, View } from "@tarojs/components";
import { useState } from "react";

const baseCls = "info-city";

const CityComp = (props) => {
    const { name, ...others } = props;
    const [region, setRegion] = useState<string[]>([]);

    return (
        <View className={`${baseCls}`}>
            <Input
                name={name}
                value={region?.join(",")}
                style={{ display: "none" }}
            />
            <LxfPicker
                {...others}
                mode="region"
                onChange={(e) => {
                    setRegion(e.detail.value);
                }}
            >
                {region?.join(",")}
            </LxfPicker>
        </View>
    );
};
export default CityComp;

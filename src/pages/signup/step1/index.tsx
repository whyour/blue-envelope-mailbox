import './index.scss';

import classNames from 'classnames';
import { useEffect, useRef, useState } from 'react';
import { Form, Text, View } from '@tarojs/components';

import { useApply, withUser } from '@/hooks';
import { goToPage, message } from '@/utils';
import { LxfButton, LxfCard, LxfCheckboxGroup } from '@/components';
import api from '@/api';
import { IApplyInfo, IFormItem } from '@/models/apply';
import { form2Comp } from '../util';
import { LxfConfirmModal } from '@/components/lxf-confirm-model';
import { IFloatWrapperRef } from '@/components/lxf-float-wrapper';
import Taro from '@tarojs/taro';
import { IApplyResp } from '@/service/type';
import { STORAGE_GENERAL_INFO_KEY } from '@/constants';

const baseCls = 'signup-page';

const SignUp = () => {
    const signRef = useRef<IFloatWrapperRef>(null);
    const errorRef = useRef<IFloatWrapperRef>(null);
    const passRef = useRef<IFloatWrapperRef>(null);
    const cannotPassRef = useRef<IFloatWrapperRef>(null);
    const [apply] = useApply();

    const [agree, setAgree] = useState<boolean>(false);
    const [province, setProvince] = useState<string>();
    const [city, setCity] = useState<string>();
    const [formItemList, setFormItemList] = useState<IFormItem[]>();
    const [formValues, setFormValues] = useState<any>();
    const [step1Res, setStep1Res] = useState<IApplyResp>();
    const { step1ApplyInfo = {} } = apply ?? {};
    const { form_description } = step1ApplyInfo as IApplyInfo;

    const handleConfirm = async () => {
        const info = formValues;

        Object.entries(info).forEach(([key, val]) => {
            const reg = /^([a-zA-Z]*)\[([0-9]+)\]$/;
            const value = val && !isNaN(Number(val)) ? Number(val) : val;
            if (reg.test(key)) {
                const keys = key.match(reg) ?? [];
                if (info[keys[1]]) {
                    info[keys[1]][keys[2]] = value;
                } else {
                    info[keys[1]] = { [keys[2]]: value };
                }
                delete info[key];
            } else if (key === 'city' && Array.isArray(value)) {
                // info[key] = value.join(',')
                info[key] = [value[0], value[1].substring(0, value[1].length - 1)].join(',');
            } else if (key === 'university') {
                info[key] = value;
            } else {
                info[key] = value;
            }
        });
        const params = { ...info };
        const hide = await message.loading('提交中');
        const res = await api.lxf.postVolunteerApplyStep1Answer(params);
        hide();
        if (res.status === 1) {
            if (res.all_score) {
                setStep1Res(res);
                if (res.score >= res.pass_score) {
                    passRef.current?.open();
                } else {
                    cannotPassRef.current?.open();
                }
            } else {
                if (res.applicat_status === '1') {
                    goToPage('/pages/signup/step2/video/index', {
                        method: 'redirectTo'
                    });
                } else {
                    goToPage('/pages/in-application/index', {
                        method: 'redirectTo'
                    });
                }
            }
            const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
            Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
                ...data,
                videoVisited: false
            });
        } else {
            errorRef.current?.open({ title: res.info });
        }
    };

    const handleSignUp = (e: any) => {
        setFormValues(e.target.value);
        signRef.current?.open();
    };

    const handleFormItemChange = (e: any, name: string) => {
        const value = e.detail.value;
        setFormItemList((prev) => {
            const newList = prev?.map((item) => {
                const show = item?.skip_rule?.show;
                if (show?.[name]) {
                    if (show[name] == value) {
                        return { ...item, show: true };
                    } else {
                        return { ...item, show: false };
                    }
                } else {
                    return item;
                }
            });
            return newList;
        });
    };

    const resetData = () => {
        const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
            ...data,
            subscribed: false
        });
    };

    useEffect(() => {
        const run = async () => {
            const hide = await message.loading('加载中');
            if (!apply.step1ApplyInfo) {
                await apply.getStep1ApplyInfo();
            }
            const { step1ApplyInfo = {} } = apply ?? {};
            const { form_item_list } = step1ApplyInfo as IApplyInfo;
            setFormItemList(form_item_list);
            hide();
        };
        run();
        resetData();
    }, []);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={'lxf_h1_text'}>一、个人信息注册</View>
                <View className={`${baseCls}-info`}>
                    <View className={classNames('lxf_info_text')}>
                        <View className="taro_html" dangerouslySetInnerHTML={{ __html: form_description }} />
                    </View>
                    <Form onSubmit={handleSignUp} className={`${baseCls}-info`}>
                        {formItemList?.map((item) => {
                            if (!item) return null;
                            const {
                                title,
                                required,
                                type,
                                name,
                                options: _options,
                                other_limit,
                                show
                            } = item;
                            let limit = 0;
                            try {
                                const limits = JSON.parse(other_limit || '{}');
                                if (limits?.min_limit) {
                                    limit = parseInt(limits?.min_limit, 10);
                                }
                            } catch (error) {}
                            const options = Array.isArray(_options)
                                ? _options.map((opt) => ({
                                      label: opt,
                                      value: opt
                                  }))
                                : typeof _options === 'object'
                                ? Object.entries(_options)?.map(([key, value]) => ({
                                      label: value,
                                      value: key
                                  }))
                                : [];
                            const Comp = form2Comp(type);
                            if (show === false) return null;
                            return (
                                <LxfCard title={`${title}${Number(required) === 1 ? '*' : ''}`}>
                                    {Comp ? (
                                        <Comp
                                            mode={type}
                                            name={name}
                                            options={options}
                                            limit={type === 'picture' ? 1 : limit}
                                            multiple={type === 'pictures'}
                                            placeholder={`请输入${title}`}
                                            province={province}
                                            city={city}
                                            setCity={setCity}
                                            setProvince={setProvince}
                                            onChange={(e: any) => {
                                                handleFormItemChange(e, name);
                                            }}
                                        />
                                    ) : null}
                                </LxfCard>
                            );
                        })}
                        {Boolean(formItemList?.length) && (
                            <LxfCheckboxGroup
                                onChange={(e) => setAgree(e.target.value.includes('agree'))}
                                options={[
                                    {
                                        label: (
                                            <>
                                                我已阅读同意并遵守
                                                <Text
                                                    className="link_text"
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                        e.preventDefault();
                                                        goToPage('/pages/agreements/index');
                                                    }}
                                                >
                                                    服务协议
                                                </Text>{' '}
                                            </>
                                        ),
                                        value: 'agree'
                                    }
                                ]}
                            />
                        )}
                        <View className={`${baseCls}-submit-btn`}>
                            <LxfButton title="提交" formType="submit" disabled={!agree} />
                        </View>
                    </Form>
                </View>
            </View>
            <LxfConfirmModal
                ref={signRef}
                title={'确定提交注册信息？'}
                showCancelBtn
                showOkBtn
                okText={'提交'}
                onOk={handleConfirm}
            />
            <LxfConfirmModal
                ref={passRef}
                title={
                    <>
                        <View>恭喜你通过考试</View>
                        {/* <View>请耐心等待审核结果</View> */}
                    </>
                }
                showOkBtn
                okText={'我知道了'}
                componentProps={{
                    content: `你的分数为${step1Res?.score}分，高于${step1Res?.pass_score}分`
                }}
                onOk={() =>
                    goToPage('/pages/signup/step2/video/index', {
                        method: 'redirectTo'
                    })
                }
            />
            <LxfConfirmModal
                ref={cannotPassRef}
                title={'分数不足，未能通过考试'}
                showOkBtn
                okText={
                    <LxfButton style={{ marginBottom: 20, width: 'calc(100% - 40px)' }} title="继续答题" />
                }
                componentProps={{
                    content: `抱歉，您的分数为${step1Res?.score}分，低于${step1Res?.pass_score}份，请重新答题提交！`
                }}
            />
            <LxfConfirmModal
                ref={errorRef}
                showOkBtn
                okText="确定"
                onOk={() => {
                    errorRef.current?.close();
                }}
            />
        </View>
    );
};
export default withUser({
    auto: true,
    inject: true
})(SignUp);

import './index.scss';

import React, { useEffect, useState } from 'react';
import { Image, Swiper, SwiperItem, Text, View } from '@tarojs/components';
import { goToPage, message } from '@/utils';
import { LxfButton } from '@/components';
import { STORAGE_GENERAL_INFO_KEY } from '@/constants';
import Taro, { useDidShow } from '@tarojs/taro';

const baseCls = 'index-page';

const imageList = [
    'https://tt642f1292fb1394a001-env-as0wwifwof.tos-cn-beijing.volces.com/welcome1.png',
    'https://tt642f1292fb1394a001-env-as0wwifwof.tos-cn-beijing.volces.com/welcome2.png'
];
const Welcome = () => {
    const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
    const [images, setImages] = useState<string[]>([]);

    const register = () => {
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
            ...data,
            joined: true
        });
        goToPage('/pages/index/index', { method: 'redirectTo' });
    };

    useDidShow(() => {
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
            ...data,
            joined: false
        });
        try {
            Taro.hideHomeButton();
        } catch (error) {}
    });

    useEffect(() => {
        const run = async () => {
            try {
                const hide = await message.loading('加载中');
                const res = await Promise.all(
                    imageList.map((x) =>
                        Taro.getImageInfo({
                            src: x
                        })
                    )
                );
                setImages(res.map((x) => x.path));
                hide();
            } catch (error) {}
        };
        run();
    }, []);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <Swiper
                    className={`${baseCls}-swiper`}
                    indicatorActiveColor={'rgba(22, 24, 35, 0.15)'}
                    indicatorColor={'#F3F3F4'}
                    adjustHeight={'highest'}
                    circular
                    indicatorDots
                    autoplay
                    previousMargin="20px"
                    nextMargin="10px"
                >
                    {images.map((image) => (
                        <SwiperItem className={`${baseCls}-swiper-card`}>
                            <Image src={image} lazyLoad />
                        </SwiperItem>
                    ))}
                </Swiper>
                <View className={`${baseCls}-footer`}>
                    <LxfButton
                        className={`${baseCls}-signbtn`}
                        title={'报名成为蓝信封通信大使'}
                        onClick={register}
                    />
                    <Text className={`${baseCls}-signdesp`}>已有超过十万志愿者加入蓝信封</Text>
                </View>
            </View>
        </View>
    );
};
export default Welcome;

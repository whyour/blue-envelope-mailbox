import './index.scss';

import classNames from 'classnames';
import { View, Image } from '@tarojs/components';
import IconInApplition from '@/assets/images/index/heartshield.png';
import { LxfButton } from '@/components';
import Taro, { useDidShow } from '@tarojs/taro';
import { useGlobal } from '@/hooks';
import { STORAGE_GENERAL_INFO_KEY } from '@/constants';
import { message } from '@/utils';
import { toJS } from 'mobx';
import { WrapModel } from '@/models';
import { useState } from 'react';

const baseCls = 'in-application';

const InApplition = () => {
    const [{ messageTemplate }] = useGlobal();
    const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
    const [subscribed, setSubscribed] = useState(data.subscribed);

    useDidShow(() => {
        Taro.hideHomeButton();
    });

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <Image className={`${baseCls}-wrapper-icon`} src={IconInApplition} />
                <View className={classNames('lxf_h2_text_2')}>您已成功提交考核申请资料</View>
                <View className={classNames('lxf_h3_text_2')}>
                    项目人员正在进行审核中，预计7-14个工作日予以申请结果通知，请耐心等待
                </View>
                <LxfButton
                    className={`${baseCls}-wrapper-subscribe-btn`}
                    title={subscribed ? '已订阅' : '订阅审核结果'}
                    disabled={subscribed}
                    onClick={() => {
                        Taro.requestSubscribeMessage({
                            tmplIds: toJS(messageTemplate).permanent,
                            success() {
                                message.success('订阅成功');
                                Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {
                                    ...data,
                                    subscribed: true
                                });
                                setSubscribed(true);
                            },
                            fail(res) {
                                message.error(`订阅失败 ${res.errMsg}`);
                            }
                        });
                    }}
                />
                <LxfButton
                    className={`${baseCls}-wrapper-share-btn`}
                    title="让更多人来参与"
                    openType="share"
                />
            </View>
        </View>
    );
};
export default WrapModel(InApplition);

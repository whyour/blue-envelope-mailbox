import React, { FunctionComponent, useEffect, useState } from "react";
import { View, WebView } from "@tarojs/components";
import { withUser } from "@/hooks";

import "./index.scss";
import api from "@/api";
import { message } from "@/utils";

const baseCls = "consult";

const Consult: FunctionComponent<{}> = () => {
    const [url, setUrl] = useState("");

    const getUdeskHelp = async () => {
        const hide = await message.loading('加载中');
        const res = await api.lxf.getUdeskHelp();
        setUrl(res.url);
        hide();
    };

    useEffect(() => {
        getUdeskHelp();
    }, []);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <WebView src={url} type="im" />
            </View>
        </View>
    );
};

export default withUser({
    auto: true,
    inject: false,
})(Consult);

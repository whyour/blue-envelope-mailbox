import './index.scss';

import { FunctionComponent } from 'react';
import { Text, View } from '@tarojs/components';

import { LxfFloatWrapper } from '@/components';

const baseCls = 'login-agreement';

const LoginAgreement: FunctionComponent<{}> = () => {
    return (
        <View className={baseCls}>
            <View className={`${baseCls}-row`}>
                <Text>获取你的抖音头像、昵称信息</Text>
            </View>
        </View>
    );
};

export default LoginAgreement;

const defaultProps = {
    title: '蓝信封邮筒'
};

export const LoginContentDrawer = LxfFloatWrapper.withDrawer<{}>({
    defaultProps
})(LoginAgreement);

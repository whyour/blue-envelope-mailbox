import './index.scss';

import React, { FunctionComponent, useCallback, useEffect, useRef, useState } from 'react';
import { View } from '@tarojs/components';

import models, { IVolunteerInfo, WrapModel } from '@/models';
import { goToPage, handleUserRouterError, message } from '@/utils';

import { LoginContentDrawer } from './components/content-drawer';
import { STORAGE_GENERAL_INFO_KEY } from '@/constants';
import Taro from '@tarojs/taro';
import { useUser } from '@/hooks';

export type IFloatWrapperBaseProps = {
    /** 标题 */
    title?: React.ReactNode;
};

export type IFloatWrapperOpenProps<P = {}> = IFloatWrapperBaseProps & {
    /** 内容属性 */
    props?: P;
};

export type IFloatWrapperRef<P = {}> = {
    /** 浮层弹窗打开接口 */
    open: (openProps?: IFloatWrapperOpenProps<P>) => void;
    /** 浮层弹窗关闭接口 */
    close: () => void;
};

const baseCls = 'login-page';

const Login: FunctionComponent<{}> = () => {
    const agreementRef = useRef<IFloatWrapperRef>(null);
    const [{ canAutoLogin }] = useUser();
    const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};

    const afterLogin = useCallback((res?: IVolunteerInfo) => {
        message.success('登录成功').then(() => {
            if (!res && !data?.joined) {
                goToPage('/pages/welcome/index', {
                    method: 'redirectTo'
                });
            } else {
                goToPage('/pages/index/index', {
                    method: 'redirectTo'
                });
            }
        });
    }, []);

    const handleLogin = useCallback(async () => {
        const hide = await message.loading('登录中');

        try {
            const res = await models.user.login();
            afterLogin(res);
        } catch (err) {
            handleUserRouterError(err);
        } finally {
            hide();
        }
    }, []);

    useEffect(() => {
        const fn = async () => {
            if (canAutoLogin) {
                const hide = await message.loading('登录中');

                try {
                    const res = await models.user.autoLogin();
                    afterLogin(res);
                } catch (err) {
                    handleUserRouterError(err);
                } finally {
                    hide();
                }
            } else {
                agreementRef.current?.open();
            }
        };

        fn();
    }, [canAutoLogin]);

    return (
        <View className={baseCls}>
            <LoginContentDrawer
                ref={agreementRef}
                title="蓝信封邮筒"
                maskClosable
                okText="同意"
                showOkBtn
                onOk={() => {
                    handleLogin();
                }}
                cancelText="不同意"
                showCancelBtn
            />
        </View>
    );
};

export default WrapModel(Login);

import './index.scss';

import React, { FunctionComponent, useMemo, useState } from 'react';
import { View } from '@tarojs/components';

import api from '@/api';
import { withUser } from '@/hooks';
import { goToPage, message } from '@/utils';
import { LxfButton } from '@/components';
import { AtInput } from 'taro-ui';
import Taro from '@tarojs/taro';

const baseCls = 'mobile-modify';

const MobileModify: FunctionComponent<{}> = () => {
    const [info, setInfo] = useState({
        disabled: false,
        second: 60
    });
    const [phoneNumber, setPhoneNumber] = useState<string>();
    const [code, setCode] = useState<string>();

    const onSave = async () => {
        if (!code || !phoneNumber) return;
        const hide = await message.loading('提交中');
        const res = await api.lxf.updateMobileNumber({
            mobile: phoneNumber,
            code
        });
        hide();
        if (res.status === 1) {
            setTimeout(() => {
                Taro.navigateBack();
            }, 300);
        } else {
            message.error(res?.info);
        }
    };

    const sendCode = async () => {
        if (info.disabled || !phoneNumber) return;
        setInfo({ ...info, disabled: true });
        const timer = setInterval(() => {
            setInfo((pre) => {
                if (pre.second > 0) {
                    return { ...pre, second: pre.second - 1 };
                } else {
                    clearInterval(timer);
                    return { disabled: false, second: 60 };
                }
            });
        }, 1000);
        const hide = await message.loading('发送中');
        await api.lxf.sendValidateSms({ mobile: phoneNumber });
        hide();
    };

    const showTipText = useMemo(() => {
        return info.disabled ? `${info.second}s后重试` : '发送验证码';
    }, [info]);

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <AtInput
                    name="phone"
                    type="phone"
                    title="手机号"
                    clear
                    placeholder="请输入手机号码"
                    value={phoneNumber}
                    onChange={(e: number) => {
                        setPhoneNumber(String(e));
                    }}
                >
                    <View
                        style={{
                            color: info.disabled ? '#FF4949' : '',
                            fontSize: '12px',
                            width: '90px'
                        }}
                        onClick={sendCode}
                    >
                        {showTipText}
                    </View>
                </AtInput>
                <AtInput
                    name="code"
                    title="验证码"
                    type="text"
                    clear
                    placeholder="验证码"
                    value={code}
                    onChange={(v: string) => {
                        setCode(v);
                    }}
                />
                <View className={`${baseCls}-savebtn`}>
                    <LxfButton title={'保存'} disabled={!phoneNumber || !code} onClick={onSave} />
                </View>
            </View>
        </View>
    );
};

export default withUser({
    auto: true,
    inject: true
})(MobileModify);

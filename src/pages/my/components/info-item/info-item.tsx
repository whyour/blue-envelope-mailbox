import './index.scss'

import React from 'react'
import { Image, ITouchEvent, Text, View } from '@tarojs/components'

import IconArrowRight from './arrow.svg'

const baseCls = 'info-item';

interface InfoItemProps{
    title: string;
    showIcon?: boolean;
    content: React.ReactNode;
    onClick?: (event: ITouchEvent) => void;
}

const InfoItem = (props: InfoItemProps) => {
    const { title, showIcon = false, onClick, content } = props;
    return (
        <View className={`${baseCls}`} onClick={onClick}>
            <Text className={`${baseCls}-title`}>{title}</Text>
            <View className={`${baseCls}-content`}>
                <View className={`${baseCls}-content-text`}>{content || '-'}</View>
                {showIcon && <Image className={`${baseCls}-content-icon`} src={IconArrowRight}></Image>}
            </View>
        </View>)
}
export default InfoItem

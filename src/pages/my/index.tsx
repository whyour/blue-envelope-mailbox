import './index.scss';

import React, { FunctionComponent } from 'react';
import { Image, Text, View } from '@tarojs/components';

import IconArrowRight from '@/assets/icons/arrow-right-2.svg';
import IconMail from '@/assets/icons/mail.svg';
import { useUser } from '@/hooks';
import { goToPage } from '@/utils';
import dayjs from 'dayjs';
import { useDidShow } from '@tarojs/taro';
import Taro from '@tarojs/taro';
import { WrapModel } from '@/models';

const baseCls = 'profile-page';
const userCls = `${baseCls}-user`;

const SelectItem = (props) => {
    const { title, onClick } = props;

    return (
        <View className={`${userCls}-select-item`} onClick={onClick}>
            <Text className={`${userCls}-select-item-title`}>{title}</Text>
            <Image className={`${userCls}-select-item-icon`} src={IconArrowRight}></Image>
        </View>
    );
};

const Profile: FunctionComponent<{}> = () => {
    const [{ userInfo }] = useUser();
    const { name, create_date } = userInfo ?? {};
    const [year, month, day] = create_date?.split(' ')[0]?.split('-') ?? [];
    const desc = [
        `${year}.${month}.${day} 成为志愿者`,
        <>
            已寄出 {userInfo?.letter_v_count || 0} 封信
            <Image style={{ width: 18, height: 18 }} src={IconMail} />
        </>
    ];

    useDidShow(() => {
        if (userInfo) {
            useUser()[0].getProfile(true);
        }
        try {
            Taro.hideHomeButton();
        } catch (error) {}
    });

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={`${userCls}-info`}>
                    <View className={`${userCls}-info-avatar`}>
                        <Image className={`${userCls}-info-avatar-img`} src={userInfo?.avatar_url || ''} />
                    </View>
                    <View className={`${userCls}-info-right`}>
                        <View className={`${userCls}-info-name`}>
                            {name}（已加入 {dayjs().diff(dayjs(create_date), 'day')} 天）
                        </View>
                        {desc.map((text) => (
                            <View className={`${userCls}-info-desc`}>{text}</View>
                        ))}
                    </View>
                </View>
                <View className={`${userCls}-select`}>
                    <SelectItem
                        title={'个人资料'}
                        onClick={() => {
                            goToPage('/pages/my/person/index');
                        }}
                    />
                    {/* <SelectItem
                        title={"志愿者证书"}
                        onClick={() => {
                            goToPage("/pages/certificate/index");
                        }}
                    /> */}
                    <SelectItem
                        title={'求助咨询'}
                        onClick={() => {
                            goToPage('/pages/consult/index');
                        }}
                    />
                </View>
            </View>
        </View>
    );
};

export default WrapModel(Profile);

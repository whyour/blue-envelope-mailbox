import "./index.scss";

import React, { FunctionComponent } from "react";
import { Image, View } from "@tarojs/components";

import { useUser, withUser } from "@/hooks";
import { goToPage } from "@/utils";

import InfoItem from "../components/info-item/info-item";
import { useDidShow } from "@tarojs/taro";
import { WrapModel } from "@/models";

const baseCls = "person-page";

const Person: FunctionComponent<{}> = () => {
    const [{ userInfo }] = useUser();
    const { name, mobile, gender, birthday, email, school } = userInfo ?? {};

    useDidShow(() => {
        if (userInfo) {
            useUser()[0].getProfile(true);
        }
    });

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={`${baseCls}-avatar`}>
                    <Image
                        className={`${baseCls}-avatar-img`}
                        src={userInfo?.avatar_url || ""}
                    />
                </View>
                <View className={`${baseCls}-select`}>
                    <InfoItem title={"姓名"} content={name} />
                    <InfoItem
                        title={"性别"}
                        content={gender === "male" ? "男" : "女"}
                    />
                    <InfoItem title={"生日"} content={birthday} />
                    <InfoItem
                        title={"手机"}
                        content={mobile}
                        onClick={() => {
                            goToPage("/pages/my/mobile-modify/index");
                        }}
                        showIcon
                    />
                    <InfoItem title={"邮箱"} content={email} />
                    <InfoItem title={"学历"} content={school} />
                </View>
            </View>
        </View>
    );
};

export default WrapModel(Person);

import React, { FunctionComponent } from "react";
import { View } from "@tarojs/components";
import { withUser } from "@/hooks";

import "./index.scss";

const baseCls = "certificate";

const Certificate: FunctionComponent<{}> = () => {
    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={`${baseCls}-wrapper-text`}>
                    请结业后再来领取
                </View>
            </View>
        </View>
    );
};

export default withUser({
    auto: true,
    inject: false,
})(Certificate);

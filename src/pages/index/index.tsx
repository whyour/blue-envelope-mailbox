import './index.scss';

import { useEffect, useState } from 'react';
import { View } from '@tarojs/components';

import { EApplicatStatus, EVolunteerStatus } from '@/constants/volunteer';
import { useGlobal, useUser, withUser } from '@/hooks';
import { goToPage, message } from '@/utils';
import { LxfPanel } from '@/components';
import Taro, { useDidShow } from '@tarojs/taro';
import { STORAGE_DISUSE_INFO_KEY, STORAGE_GENERAL_INFO_KEY } from '@/constants';
import api from '@/api';
import { IVolunteerInfo } from '@/models';

const baseCls = 'signup-page';

const Index = () => {
    const [{ userInfo }] = useUser();
    const [step, setStep] = useState<number>();
    const storageGeneral = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};

    const handleBindMobile = async (e) => {
        if (e.detail.errNo === 10200) {
            // message.error(e.detail.errMsg, 1500);
        } else {
            const hide = await message.loading('加载中');
            const res = await api.lxf.updateVolunteerInfo(e.detail);
            hide();
            if (res.status === 0) {
                message.error(res.info);
                return;
            }
            checkUserStatus(res.data, true);
        }
    };

    useDidShow(() => {
        if (userInfo) {
            useUser()[0].getProfile(true);
        }
        try {
            Taro.hideHomeButton();
        } catch (error) {}
    });

    const checkUserStatus = (userInfo: IVolunteerInfo, directToPage = false) => {
        const status = userInfo?.status;
        if (status === EVolunteerStatus.DISUSE) {
            Taro.setStorageSync(STORAGE_DISUSE_INFO_KEY, 'eliminated-user');
            goToPage('/pages/disuse/index', { method: 'redirectTo' });
            return;
        } else if (status === EVolunteerStatus.NORMAL) {
            goToPage('/pages/mailbox/index', { method: 'redirectTo' });
            return;
        } else if (status === EVolunteerStatus.DISABLED || userInfo.info?.includes('已被禁用')) {
            Taro.setStorageSync(STORAGE_DISUSE_INFO_KEY, 'disabled-user');
            goToPage('/pages/disuse/index', { method: 'redirectTo' });
            return;
        }
        // 0,1_2 跳第一阶段申请;
        // 1_1,2_2 跳第二阶段申请;
        // 2_1,3_2 跳第三阶段申请;
        // 1_0,2_0,3_0 跳申请审核中
        // 3_1 跳邮箱
        const applicat_status = userInfo?.applicat_status;

        if (
            [
                EApplicatStatus.STEP_1_IN_APPLICATION,
                EApplicatStatus.STEP_2_IN_APPLICATION,
                EApplicatStatus.STEP_3_IN_APPLICATION
            ].includes(applicat_status)
        ) {
            goToPage('/pages/in-application/index', { method: 'redirectTo' });
        } else if (applicat_status === EApplicatStatus.STEP_3_PASSED) {
            goToPage('/pages/mailbox/index', { method: 'redirectTo' });
        } else if (
            [
                EApplicatStatus.STEP_1_RETURN,
                EApplicatStatus.STEP_2_RETURN,
                EApplicatStatus.STEP_3_RETURN
            ].includes(applicat_status)
        ) {
            const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
            if (!data.reenroll) {
                Taro.setStorageSync(STORAGE_DISUSE_INFO_KEY, 'returned-user');
                goToPage('/pages/disuse/index', { method: 'redirectTo' });
            } else {
                setStep(parseInt(applicat_status.split('_')?.[0]));
            }
        } else if ([EApplicatStatus.STEP_1_PASSED].includes(applicat_status)) {
            directToPage ? goToPage('/pages/signup/step2/video/index', { method: 'redirectTo' }) : setStep(2);
        } else if ([EApplicatStatus.STEP_2_PASSED].includes(applicat_status)) {
            directToPage ? goToPage('/pages/signup/step3/index', { method: 'redirectTo' }) : setStep(3);
        } else {
            directToPage ? goToPage('/pages/signup/step1/index', { method: 'redirectTo' }) : setStep(1);
        }
    };

    useEffect(() => {
        if (!userInfo) {
            setStep(1);
            return;
        }
        checkUserStatus(userInfo);
    }, [userInfo]);

    useEffect(() => {
        useGlobal()[0].getMessageTemplate();
    }, []);

    if (!step) {
        return null;
    }

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={'lxf_h1_text'}>欢迎报名蓝信封通信大使</View>
                <View className={`${baseCls}-info`}>
                    <View className={'lxf_info_text'}>
                        蓝信封【乡村儿童的书信笔友】项目，通过志愿者与乡村儿童一对一写信，以朋辈陪伴方式鼓励孩子在书信空间表达情感，引导其健康快乐成长！
                    </View>
                    <View className={`${baseCls}-info-desp`}>
                        <LxfPanel title={'报名时间'} content={'全年开放报名'} type={'info'} />
                        <LxfPanel
                            title={'报名要求'}
                            content={'18周岁及以上（或已获高校录取通知书）'}
                            type={'info'}
                        />
                    </View>
                    <View className={`${baseCls}-info-action`}>
                        <View className={'lxf_h3_text'}>志愿者申请流程</View>
                        <LxfPanel
                            title={'一、个人信息注册'}
                            content={'填写个人基本信息，用时约5min'}
                            buttonText={'去注册'}
                            buttonProps={
                                !userInfo?.mobile
                                    ? {
                                          openType: 'getPhoneNumber',
                                          onGetPhoneNumber: handleBindMobile
                                      }
                                    : undefined
                            }
                            onClick={
                                userInfo?.mobile
                                    ? () => {
                                          goToPage('/pages/signup/step1/index', { method: 'redirectTo' });
                                      }
                                    : undefined
                            }
                            disabled={step !== 1}
                        />
                        <LxfPanel
                            title={'二、观看书信培训视频'}
                            content={'了解书信项目，理解通信规则，用时约15min'}
                            buttonText={'去观看'}
                            onClick={() => {
                                goToPage('/pages/signup/step2/video/index', {
                                    method: 'redirectTo'
                                });
                            }}
                            disabled={step !== 2 || storageGeneral?.videoVisited}
                        />
                        <LxfPanel
                            title={'三、理论考试'}
                            content={'完成题目考试，等待审核，用时30min'}
                            buttonText={'去考试'}
                            onClick={() => {
                                goToPage(
                                    step === 3
                                        ? '/pages/signup/step3/index'
                                        : '/pages/signup/step2/exam/index',
                                    { method: 'redirectTo' }
                                );
                            }}
                            disabled={![2, 3].includes(step)}
                        />
                        <View className={'lxf_info_text'}>
                            说明：获取审核结果需要7-14个工作日，无论通过与否均会发送通知，请耐心等待。
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
};
export default withUser({
    auto: true,
    inject: true
})(Index);
